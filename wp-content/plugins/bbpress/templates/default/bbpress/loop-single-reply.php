<?php

/**
 * Replies Loop - Single Reply
 *
 * @package bbPress
 * @subpackage Theme
 */

?>



<div <?php bbp_reply_class(); ?> style="position:relative">

	<div class="bbp-reply-author">

		<?php do_action( 'bbp_theme_before_reply_author_details' ); ?>

		<?php bbp_reply_author_link( array( 'sep' => '<br />', 'show_role' => false ) ); ?>

                            <div class='media-body-text text-center'>
                            <?php bbp_topic_post_count() ?> сообщений
                            </div>
		<?php if ( bbp_is_user_keymaster() ) : ?>

			<?php do_action( 'bbp_theme_before_reply_author_admin_details' ); ?>

			<div class="bbp-reply-ip"><?php bbp_author_ip( bbp_get_reply_id() ); ?></div>

			<?php do_action( 'bbp_theme_after_reply_author_admin_details' ); ?>

		<?php endif; ?>

		<?php do_action( 'bbp_theme_after_reply_author_details' ); ?>

	</div><!-- .bbp-reply-author -->

	<div class="bbp-reply-content">

		<?php do_action( 'bbp_theme_before_reply_content' ); ?>
		<div class='post-list-item-content col-xl-10 col-lg-10 col-md-12 col-sm-12 col-12'>
		<h5 class=" media-body-header"><?php bbp_topic_title(); ?></h5>
		<div class='media-body-text'>
                                <span class='media-body-text-date'><?php bbp_topic_post_date() ?></span>
                            </div>
		<?php bbp_reply_content(); ?>
</div>
		<?php do_action( 'bbp_theme_after_reply_content' ); ?>

	</div><!-- .bbp-reply-content -->

<?php if(is_user_logged_in()){ ?>
<div class="close-btn" id="<?php bbp_reply_id(); ?>" onclick="trach_reply(<?php bbp_reply_id(); ?>)"> </div>
<?php } ?>
</div><!-- .reply -->
<div id="post-<?php bbp_reply_id(); ?>" class="bbp-reply-header">


</div><!-- #post-<?php bbp_reply_id(); ?> -->
