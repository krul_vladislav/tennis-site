<?php

/**
 * Topics Loop
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<?php do_action( 'bbp_template_before_topics_loop' ); ?>

<h2 class="header-section">ФОРУМ</h2>
        <section class="forum-page-item">
            <div class='d-flex justify-content-between align-items-center flex-wrap'>
                <h4 class='forum-item-header'><?php the_title(); ?></h4>     
				<form action="" class='forum-page-item-form col-xl-6 col-lg-6 col-md-8 col-sm-12 col-12 p-0'>
                    <label class='form-label  mb-0 mr-2'>Перейти к</label>
                    <select class="form-control col-xl-7 col-lg-7 col-md-7 col-sm-7 col-8 mr-2"  id="exampleFormControlSelect1"> 
                    <?php 
						$subforum_ids = bbp_get_all_child_ids( bbp_get_forum_parent_id(the_ID()), bbp_get_forum_post_type() );
					
                        foreach($subforum_ids as $subforum_item) {
                    ?>
                        <option value="<?= bbp_forum_permalink($subforum_item); ?>"><?php bbp_forum_title($subforum_item);?></option>                        
                    <?php
						}
						
					?>
					
                    </select>
                  
					<a id="select-href" href="<?php echo bbp_forum_permalink($subforum_ids[0]) ?>" class='content-btn' > <img src="/../../../../wp-content/themes/IM-TENNIS/img/search.png" alt=""> </a>
					
                </form>
			</div>
			</section>

<ul id="bbp-forum-<?php bbp_forum_id(); ?>" class="bbp-topics">



	<li class="bbp-body">

		<?php while ( bbp_topics() ) : bbp_the_topic(); ?>

			<?php bbp_get_template_part( 'loop', 'single-topic' ); ?>

		<?php endwhile; ?>

	</li>

	<li class="bbp-footer">

		<div class="tr">
			<p>
				<span class="td colspan<?php echo ( bbp_is_user_home() && ( bbp_is_favorites() || bbp_is_subscriptions() ) ) ? '5' : '4'; ?>">&nbsp;</span>
			</p>
		</div><!-- .tr -->

	</li>

</ul><!-- #bbp-forum-<?php bbp_forum_id(); ?> -->

<?php do_action( 'bbp_template_after_topics_loop' ); 


