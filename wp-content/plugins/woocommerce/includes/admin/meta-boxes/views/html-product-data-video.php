<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<div id="advanced_product_data" class="panel woocommerce_options_panel hidden">

	<div class="options_group hide_if_external hide_if_grouped">
		<?php
			woocommerce_wp_textarea_input(
				array(
					'id'          => '_purchase_note',
					'value'       => $product_object->get_purchase_note( 'edit' ),
					'label'       => __( 'Добавьте ссылку на видео с https://www.youtube.com : ', 'woocommerce' ),
					'desc_tip'    => true,
					'description' => __( 'Enter an optional note to send the customer after purchase.', 'woocommerce' ),
				)
			);
		?>
	</div>
	<?php do_action( 'woocommerce_product_options_advanced' ); ?>
</div>
