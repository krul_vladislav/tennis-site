<?php
/**
 * Linked product options.
 *
 * @package WooCommerce/admin
 */

defined( 'ABSPATH' ) || exit;
//echo $_GET['get_url_video'];
global $post, $woocommerce;
$product_id = get_the_ID();
global $videovalue;
	//update_post_meta( $product_id, '_product_video', $videovalue ) ;
	if(isset($_POST['video_url'])){
		update_post_meta( $product_id, '_product_video', $_POST['video_url'] ) ;
	}

	$url = get_post_meta( $product_id, '_product_video', true );


?>
<div id="video_product_data" class="panel_add_video" style="margin-top: 30px; display: none">

  <div class="first_block_video" id="form_save_url_video-form">
	<!-- <form  method="POST" id="form_save_url_video-form"> -->
			<label style="padding: 40px 40px 40px 40px">Вставьте ссылку на видео с <a href="https://www.youtube.com">https://www.youtube.com</a> :</label>
			<?php 

         
			?>
			<input name="name_url_video" type="text" id="get_url_video" size="60" placeholder="Вставьте ссылку" value="<?php echo $url ?>">
			<input class="button save_attributes button-primary" style="margin-left: 40px" type="button" id="btn_save_url_video" value="Сохранить">
		<!-- </form> -->
	</div>

<script>	
jQuery(document).ready(function($) {
		$(document).on('click','#btn_save_url_video', function(e){
			e.preventDefault ();

			var video = $('#form_save_url_video-form').find('#get_url_video').val();
			var url = window.location.href;
			
			$.ajax({
           type: "POST",
           url: url,
           data: {video_url : video},
           success: function(data)
           {
						 console.log(data);
						  // window.location.reload(true);
           }
         });

		});
	});


// document.addEventListener("DOMContentLoaded", function() {
//   var chechTag = document.getElementById("get_url_video");
//   if (chechTag) {
//     document
//       .getElementById("get_url_video")
//       .addEventListener("change", function() {
//         var videovalue = document.getElementById("get_url_video").value;
// 				console.log(videovalue);
//       });
//   }
// });
</script> 

	<?php
		// update_post_meta( $product_id, '_product_video', '111' ) ;
	do_action( 'woocommerce_product_options_related' ); ?>
</div>
