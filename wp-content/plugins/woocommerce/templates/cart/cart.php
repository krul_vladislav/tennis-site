<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;



global $woocommerce;
if(isset($_POST['clean-form']) && $_POST['clean-form'] == 'clean-form')
	 $woocommerce->cart->empty_cart();
do_action( 'woocommerce_before_cart' ); 
?>



<h2 class="header-section">ВАШ ЗАКАЗ</h2>

<section class="cart  row">
            <div class=' col-12'>
<form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
	<?php do_action( 'woocommerce_before_cart_table' ); ?>

	<table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
	<div class='d-flex mb-2'>
                <div class="col-xl-7 col-lg-7 cart-table">
                    <div class='cart-table-header offset-xl-5 offset-lg-4 col-xl-7 col-lg-8 text-left'>ТОВАР</div>
                </div>
                    <div class="col-xl-5 col-lg-5 col-md-12 d-flex">
                        <div class='cart-table-header col-4'>ЦЕНА</div>
                        <div class='cart-table-header col-4 p-0'>КОЛИЧЕСТВО</div>
                        <div class='cart-table-header col-4'>ВСЕГО</div>
                    </div>
                </div>
	
		<tbody>
			<?php do_action( 'woocommerce_before_cart_contents' ); ?>

			<?php
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
					?>
			   <div class="card cart-card d-flex align-items-center flex-row mb-2 flex-wrap">
		
				     	<div class="col-xl-7 col-lg-7 col-md-12 d-flex align-items-center flex-wrap">
                  <div style="font-size: 35px; background-color: #fdfdfd;">
										<?php
										// @codingStandardsIgnoreLine
										echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
											'<a href="%s" aria-label="%s" style="color: #e5e5e5" data-product_id="%s" data-product_sku="%s">&times;</a>',
											esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
											__( 'Remove this item', 'woocommerce' ),
											esc_attr( $product_id ),
											esc_attr( $_product->get_sku() )
										), $cart_item_key );
								     	?>
									</div>
									

                                <div class='cart-card-img col-xl-4 col-lg-3 col-md-3 col-sm-3 col-12' style='background-image: url(img/card.jpg);'>
																		<?php
																		$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
																		?>
                                 
																		<?php
																		if ( ! $product_permalink ) {
																			echo $thumbnail; // PHPCS: XSS ok.
																		} else {
																			printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail ); // PHPCS: XSS ok.
																		}
																		?>
																	
																</div>
					
                                    <div class="cart-card-text col-xl-7 col-lg-8 col-md-8 col-sm-8  col-12">
																			<?php
																			if ( ! $product_permalink ) {
																				echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' );
																			} else {
																				echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key ) );
																			}

																			do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key );

																			// Meta data.
																			echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok.

																			// Backorder notification.
																			if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
																				echo wp_kses_post( apply_filters( 'woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>', $product_id ) );
																			}
																			?>
																		</div>
				  </div>

						        <div class="col-xl-5 col-lg-5 col-md-12 d-flex">
                                    <div class=" cart-card-price col-4" style="margin-top: 12px">
										<?php
										echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
										?>
                                    </div>
											<div class="col-4 text-center" style="margin-top: -12px">
												<?php
												if ( $_product->is_sold_individually() ) {
													$product_quantity = sprintf( '1 <input id="update_card_all" type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
												} else {
													$product_quantity = woocommerce_quantity_input( array(
														
														'input_name'   => "cart[{$cart_item_key}][qty]",
														'input_value'  => $cart_item['quantity'],
														'max_value'    => $_product->get_max_purchase_quantity(),
														'min_value'    => '0',
														'product_name' => $_product->get_name(),
													), $_product, false );
												}
																 ?>
																		<script>
																		jQuery(document).ready(function($) {
																				var upd_cart_btn = $(".update-cart-button");
																				upd_cart_btn.hide();
																				$(".cart-form").find(".qty").on("change", function(){
																						upd_cart_btn.trigger("click");
																				});
																		});
																		</script>;
																		<?php
																
												echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item ); // PHPCS: XSS ok.
												?>
											</div>
                       <div class="cart-card-price col-4" style="margin-top: 12px">
											<?php
											echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
								        	?>
										</div>
                                </div>
                          
				</div>	
						
					
					
					<?php
				}
			}

			?>
	</div>
	
			<?php do_action( 'woocommerce_cart_contents' ); 
			
			?>

			<div class="col-12 d-flex justify-content-between mt-4 cart-table-bottom">
                <div class='mt-3' >
					 <a style="color: #288820;" href="#" class='cart-link' id="cart_clear"> <?php ?>Очистить корзину</a> 
					<?php do_action( 'woocommerce_cart_actions' ); ?>
					<?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>

                    <a style="color: #288820;" href="http://im-tennis.urich.org/shop/" class='cart-link cart-link-prev d-flex align-items-center mt-2'>Продолжить покупки</a>
                </div>
                <div class='text-center cart-all'>
                    <div class='cart-all-text mb-3'>Всего: <span class='cart-all-text-price'><?php  wc_cart_totals_order_total_html();  ?></span></div>
                    <a  href="http://im-tennis.urich.org/checkout/" class='content-btn' id="content-btn2">ОФОРМИТЬ ЗАКАЗ</a>
                </div>
            </div>

			

					<button type="submit" id="update_card" class="button" style="opacity:0" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><?php esc_html_e( 'Update cart', 'woocommerce' ); ?></button>

					<?php do_action( 'woocommerce_cart_actions' ); ?>

					<?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>
			

			<?php do_action( 'woocommerce_after_cart_contents' ); ?>
		</tbody>
	</table>
	<?php do_action( 'woocommerce_after_cart_table' ); ?>
</form>

<div class="cart-collaterals">
	<?php
		/**
		 * Cart collaterals hook.
		 *
		 * @hooked woocommerce_cross_sell_display
		 * @hooked woocommerce_cart_totals - 10
		 */
		do_action( 'woocommerce_cart_collaterals' );


	?>
</div>
</section>
<?php do_action( 'woocommerce_after_cart' ); ?>

<form method='POST' id='form-cart-clean' autocomplete='off'>
	<input type='hidden' name='clean-form' value='clean-form'>
</form>

<script>		
jQuery(document).ready(function($) {
		$(document).on('click','#cart_clear', function(e){
			e.preventDefault ();

			var form = $('#form-cart-clean');
			// var url = form.attr('action');

			$.ajax({
           type: "POST",
          //  url: url,
           data: form.serialize(), // serializes the form's elements.
           success: function(data)
           {
               window.location.reload(true);
           }
         });

		});
	});


	// 	console.log(['1111111') 
  // if (chechTag) {
	// 	document
	
  //     .getElementById("cart_clear")
  //     .addEventListener("click", function() {
	// 			$korzina->empty_cart( $clear_persistent_cart = true ); 
  //     });
	// }

	// $('.input-text.qty.text').each(function(item){
	//  console.log([item].id) 
	// });




</script>