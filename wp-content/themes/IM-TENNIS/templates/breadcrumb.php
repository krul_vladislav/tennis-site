<?php
/*
 * Breadcrumb template loop
 */

?>

<nav aria-label="breadcrumb">
<?php //echo '<pre>'.$_SERVER['REQUEST_URI'].'</pre>'; ?>
    <ol class="breadcrumb">

        <?php if (is_404()): ?>

            <li class="breadcrumb-item"><a href="<?php echo get_home_url(); ?>/shop/"><?php _e('Магазин', THEME_OPT) ?></a></li>
            <li class="breadcrumb-item active" aria-current="page">404</li>

        <?php elseif (is_search()): ?>

        <?php elseif ($_SERVER['REQUEST_URI'] == '/blog/'): ?>
            <li class="breadcrumb-item"><a href="<?php echo get_home_url(); ?>/blog/"><?php _e('Блог', THEME_OPT) ?></a></li>

        <?php elseif ($_SERVER['REQUEST_URI'] == '/kontakty/'): ?>
            <li class="breadcrumb-item"><a href="<?php echo get_home_url(); ?>/kontakty/"><?php _e('Контакты', THEME_OPT) ?></a></li>

        <?php elseif ($_SERVER['REQUEST_URI'] == '/o-nas/'): ?>
            <li class="breadcrumb-item"><a href="<?php echo get_home_url(); ?>/o-nas/"><?php _e('О нас', THEME_OPT) ?></a></li>

        <?php elseif (is_account_page() || is_page_template('registration-page.php')): ?>

            <li class="breadcrumb-item"><a href="<?php echo get_home_url(); ?>/shop/"><?php _e('Магазин', THEME_OPT) ?></a></li>
            <li class="breadcrumb-item active" aria-current="page"><?php _e('Войти в кабинет', THEME_OPT) ?></li>

        <?php elseif (is_search()): ?>

            <li class="breadcrumb-item"><a href="<?php echo get_home_url(); ?>/shop/"><?php _e('Магазин', THEME_OPT) ?></a></li>
            <li class="breadcrumb-item active" aria-current="page"><?php _e('Результаты поиска', THEME_OPT) ?></li>

        <?php elseif (is_single()): ?>

            <!-- <li class="breadcrumb-item"><a href="<?php echo get_home_url(); ?>/shop/"><?php _e('Магазин', THEME_OPT) ?></a></li> -->
            <li class="breadcrumb-item"><a href="<?php echo get_home_url(); ?>/blog/"><?php _e('Блог', THEME_OPT) ?></a></li>
            <li class="breadcrumb-item active" aria-current="page"><?php the_title(); ?></li>

        <?php else: ?>

            <li class="breadcrumb-item"><a href="<?php echo get_home_url(); ?>/shop/"><?php _e('Магазин', THEME_OPT) ?></a></li>
            <?php if ($_SERVER['REQUEST_URI'] == '/product-category/odezhda/'): ?>
            <li class="breadcrumb-item active2" aria-current="page"><a href="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>"><?php echo 'Одежда'; ?></a></li>
           
            <?php elseif ($_SERVER['REQUEST_URI'] == '/product-category/mjachi/'): ?>
            <li class="breadcrumb-item active2" aria-current="page"><a href="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>"><?php echo 'Мячи'; ?></a></li>
          
            <?php elseif ($_SERVER['REQUEST_URI'] == '/product-category/nakladki/'): ?>
            <li class="breadcrumb-item active2" aria-current="page"><a href="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>"><?php echo 'Накладки'; ?></a></li>
          
            <?php elseif ($_SERVER['REQUEST_URI'] == '/product-category/obuv/'): ?>
            <li class="breadcrumb-item active2" aria-current="page"><a href="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>"><?php echo 'Обувь'; ?></a></li>
         
            <?php elseif ($_SERVER['REQUEST_URI'] == '/product-category/osnovanija/'): ?>
            <li class="breadcrumb-item active2" aria-current="page"><a href="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>"><?php echo 'Основание'; ?></a></li>
          
            <?php elseif ($_SERVER['REQUEST_URI'] == '/product-category/raketki/'): ?>
            <li class="breadcrumb-item active2" aria-current="page"><a href="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>"><?php echo 'Ракетки'; ?></a></li>
           
            <?php elseif ($_SERVER['REQUEST_URI'] == '/product-category/setki/'): ?>
            <li class="breadcrumb-item active2" aria-current="page"><a href="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>"><?php echo 'Сетки'; ?></a></li>

            <?php elseif ($_SERVER['REQUEST_URI'] == '/product-category/sumki/'): ?>
            <li class="breadcrumb-item active2" aria-current="page"><a href="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>"><?php echo 'Сумки'; ?></a></li>
          
            <?php elseif ($_SERVER['REQUEST_URI'] == '/product-category/tejpirovanie/'): ?>
            <li class="breadcrumb-item active2" aria-current="page"><a href="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>"><?php echo 'Тейпирование'; ?></a></li>
           
            <?php elseif ($_SERVER['REQUEST_URI'] == '/product-category/chehly/'): ?>
            <li class="breadcrumb-item active2" aria-current="page"><a href="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>"><?php echo 'Чехлы'; ?></a></li>

            <?php elseif ($_SERVER['REQUEST_URI'] == '/forum/'): ?>
            <li class="breadcrumb-item active2" aria-current="page"><a href="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>"><?php echo 'Форум'; ?></a></li>
            <?php endif; ?>
            
        <?php endif; ?>
    </ol>
</nav>

