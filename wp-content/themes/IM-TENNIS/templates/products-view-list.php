<?php
if (have_posts()): while (have_posts()) : the_post();

            $product = wc_get_product(get_the_ID());
            $link = get_the_permalink();
            $product_name = $product->get_name();
            $price = $product->get_regular_price() . ' ' . __('грн', THEME_OPT);
            ($product->get_sale_price()) ? $sale_price = $product->get_sale_price() . ' ' . __('грн', THEME_OPT) : $sale_price = null;
            if (has_post_thumbnail()) $product_image = get_the_post_thumbnail_url(get_the_ID(), 'medium');
            $product_buy = $product->add_to_cart_url();
            $product_short_desc = $product->get_short_description();
            $tags = get_the_terms(get_the_ID(), 'product_tag');
            (is_object($tags[0])) ? $tag = __($tags[0]->name, THEME_OPT) : $tag = null;
            if ($tag == 'Хит') $tag_class = 'popular-product-label';
            elseif ($tag == 'Новинка') $tag_class = 'new-products-label';
            elseif ($tag == 'Под заказ') $tag_class = 'order-products-label';
            elseif ($tag == 'Акция') {
                $tag_class = 'promotion-products-label';
                $tag = round(($price - $sale_price) / $price * 100) . '%';
            }
         //   include_once 'E:\OpenServer\domains\im-tennis.urich.org\wp-content\plugins\woocommerce\includes\admin\meta-boxes\class-wc-meta-box-coupon-data.php';
            ?>

                <div class="card flex-row flex-wrap mb-2" style="min-height:130px">
						<a href="<?php echo get_the_permalink(get_the_ID()); ?>" class='card-container-img col-lg-2 col-md-2 col-sm-6 col-12 p-0' style='background-image: url(<?php echo $product_image ?>);'>
							<div class="card-label popular-product-label <?php echo $tag_class ?>"><?php echo $tag ?></div>
						</a>
						<div class="card-body col-lg-6 col-md-6 col-sm-6 col-12">
							<a class="card-title mb-2 text-left"><?php echo $product_name ?></a>
                            <?php if($product_short_desc){ ?>
                            <p class="card-text text-left"><?php echo trim_characters($product_short_desc, 140); ?></p>
							<?php } ?>
						</div>

						<div class="card-footer d-flex align-items-center pb-0 pl-0 col-lg-4 col-md-4 col-sm-12 col-12">
                        <?php if (!$sale_price): ?>
                              <div class="price  mb-0 col-6 p-0"><span class='text-success d-block mb-3'><?php echo $price ?></span></div>
                        <?php else: ?>
                            <div class="price  mb-0 col-6 p-0"><span class='text-success d-block mb-3'><?php echo $sale_price ?></span>
                            <span class='price-secondary'><?php echo $price ?></span></div>
                        <?php endif; ?>
							<a href="<?php echo $product_buy ?>" class="btn card-btn d-flex justify-content-center align-items-center mb-0 col-6"><?php _e('Купить', THEME_OPT) ?></a>
						</div>
					</div>

<?php endwhile;?>


<?php endif; ?>


