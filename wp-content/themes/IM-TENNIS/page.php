<?php get_header(); ?>

<div class="container">
<ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="http://im-tennis.urich.org/shop/">Магазин</a></li>
                <!-- <li class="breadcrumb-item active" aria-current="page"><a href="<?= bbp_forum_permalink(bbp_get_forum_parent_id()); ?>"> <?php bbp_forum_title(bbp_get_forum_parent_id());?></a></li>
              -->  <li class="breadcrumb-item active" aria-current="page"> <?php the_title(); ?></li> 
            </ol>
	
 
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<?php the_content(); ?>

			<?php comments_template( '', true ); // Remove if you don't want comments ?>

		</article>
		<!-- /article -->

	<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>

			<h2><?php _e( 'Sorry, nothing to display.', THEME_OPT ); ?></h2>

		</article>
		<!-- /article -->

	<?php endif; ?>

</div>
	<!-- /section -->
	<script>
    ( function( $ ) {
        $( document ).ready(function() {  
            $(document).on('change','#exampleFormControlSelect1',function(){                
                $('#select-href').attr('href',$(this).val());
                console.log(this);

            });
        });
     } )( jQuery );       
    </script>
<?php
function cl_print_r ($var, $label = '')
{
  $str = json_encode(print_r ($var, true));
  echo "<script>console.group('".$label."');console.log('".$str."');console.groupEnd();</script>";
}
 get_footer(); ?>
