<?php // Template Name: Page forum-theme ?>

<?php get_header(); ?>

<div class="container">

<nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="http://im-tennis.urich.org/shop/">Магазин</a></li>
                <li class="breadcrumb-item"><a href="http://im-tennis.urich.org/forum/">Форум</a></li>
                <li class="breadcrumb-item"><a href="http://im-tennis.urich.org/forum-list/">Инвентарь</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php bbp_topic_title("120"); ?></li>
            </ol>
        </nav>
        <h2 class="header-section">ФОРУМ</h2>
        <section class="forum-page-item">
            <div class='d-flex justify-content-between align-items-center'>
                <div class=''>
                    <h4 class='forum-item-header mb-1'><?php bbp_forum_title("120");?></h4>
                    <div class='media-body-text '>Просмотров: <?php bbp_topic_reply_count("120");?></div>
                </div>
                <div>
                    <a href=""><img src="img/icon-forum.png" alt="" class='mr-2'></a>
                    <a href=""><img src="img/forum-fax.png" alt=""></a>
                </div>
            </div>
            <ul class="list-unstyled post-list mt-5">
                <li class='post-list-item'>
                    <div class=" d-flex flex-wrap">
                        <div class='post-list-item-info col-xl-2 col-lg-2 col-md-4 col-sm-4 col-12'>
                            <div style='background-image: url(img/bg-promotion.jpg)' class='post-list-item-info-img'><?php bbp_get_reply_author_avatar() ?></div>
                            <div class='post-list-item-info-name'><?php bbp_topic_author("120")?></div>
                            <div class='media-body-text text-center'>
                            <?php bbp_topic_post_count("120") ?> сообщений
                            </div>
                        </div>
                        <div class='post-list-item-content col-xl-10 col-lg-10 col-md-12 col-sm-12 col-12'>
                            <h5 class=" media-body-header"><?php bbp_topic_title("120"); ?></h5>
                            <div class='media-body-text '>
                                <span class='media-body-text-date '><?php bbp_topic_post_date("120") ?></span>
                            </div>
                            <div class='mt-2'>
                                <p class='post-list-item-content-text'><?php echo bbp_get_reply_id('120'); ?></p>
                                
	
                            </div>
                        </div>
                    </div>
                </li>
                <li class='post-list-item'>
                        <div class=" d-flex flex-wrap">
                            <div class='post-list-item-info col-xl-2 col-lg-2 col-md-4 col-sm-4 col-12'>
                                <div style='background-image: url(img/bg-promotion.jpg)' class='post-list-item-info-img'></div>
                                <div class='post-list-item-info-name'>Никнейм</div>
                                <div class='media-body-text text-center'>
                                    13224 сообщений
                                </div>
                            </div>
                            <div class='post-list-item-content col-xl-10 col-lg-10 col-md-12 col-sm-12 col-12'>
                                <h5 class=" media-body-header">RE: <?php bbp_topic_title("120"); ?></h5>
                                <div class='media-body-text '>
                                    <span class='media-body-text-date '>12 Дек 2018, 13:58:30</span>
                                </div>
                                <div class='mt-2'>
                                    <p class='post-list-item-content-text'> Играю обычные встречи-тренировочным
                                        Доником.Тяжелы и довольно приличный мяч.</p>
                                    <p class='post-list-item-content-text'>Более отвественные или по приколу-Нитакку.</p>
                                    <p class='post-list-item-content-text'>Хотя и среди них есть косяки.Читал по форуму
                                        зарубежному-очень котируются Стиговские
                                        Мячи.Люблю играть твердыми и тяжелыми мячами.</p>
                                    <p class='post-list-item-content-text'>Батерфляевские тренировочные-мягкие и легкие.Не
                                        очень понравились.ТСП-очень много
                                        кривых.Просто больше половины,хоть и три звезды.А так,если ровный,то ничего мячи.</p>
                                </div>
                            </div>
                        </div>
                    </li>
                <li class='post-list-item'>
                    <div class=" d-flex flex-wrap">
                        <div class='post-list-item-info col-xl-2 col-lg-2 col-md-4 col-sm-4 col-12'>
                            <div style='background-image: url(img/bg-promotion.jpg)' class='post-list-item-info-img'></div>
                            <div class='post-list-item-info-name'>Никнейм</div>
                            <div class='media-body-text text-center'>
                                13224 сообщений
                            </div>
                        </div>
                        <div class='post-list-item-content col-xl-10 col-lg-10 col-md-12 col-sm-12 col-12'>
                            <h5 class=" media-body-header">Тема обсуждаемая</h5>
                            <div class='media-body-text '>
                                <span class='media-body-text-date '>12 Дек 2018, 13:58:30</span>
                            </div>
                            <div class="post-list-item-content-message mt-3 mb-3">
                                <p class='post-list-item-content-text font-weight-bold'> Цитата: Никнейм от 20 декабря
                                    2018, 13:43:12</p>
                                <p class='post-list-item-content-text'> Играю обычные встречи-тренировочным
                                    Доником.Тяжелы и довольно приличный мяч.</p>
                                <p class='post-list-item-content-text'>Более отвественные или по приколу-Нитакку.</p>
                                <p class='post-list-item-content-text'>Хотя и среди них есть косяки.Читал по форуму
                                    зарубежному-очень котируются Стиговские
                                    Мячи.Люблю играть твердыми и тяжелыми мячами.</p>
                                <p class='post-list-item-content-text'>Батерфляевские тренировочные-мягкие и легкие.Не
                                    очень понравились.ТСП-очень много
                                    кривых.Просто больше половины,хоть и три звезды.А так,если ровный,то ничего мячи.</p>
                            </div>
                            <div class='mt-2'>
                                <p class='post-list-item-content-text'> Доник Коуч имеет ту же цену, только качество
                                    гораздо выше - зачем мне Тибхар Басик при таком раскладе? я лучше предложу людям
                                    Доники; многие, знаете ли, ценят, когда им за те же деньги предлагают более
                                    качественный товар
                                    прикол в том, что НАСТОЯЩИЕ мячи ДаблФиш отличаются по качеству от говна под
                                    названием Тибхар Басик на порядки</p>

                            </div>
                        </div>
                    </div>
                </li>

            </ul>

            <nav aria-label="Page navigation" class='d-flex justify-content-end align-items-center flex-wrap'>
                <ul class="pagination justify-content-center mb-0 col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 p-0">
                    <li class="page-item">
                        <a class="page-link page-link-arrow" href="#" aria-label="Previous">
                            <span aria-hidden="true" class=''>&laquo;</span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">4</a></li>
                    <li class="page-item"><a class="page-link" href="#">5</a></li>
                    <li class="page-item">
                        <a class="page-link page-link-arrow" href="#" aria-label="Next">
                            <span aria-hidden="true" class=''>&raquo;</span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                </ul>
                <div class='col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 p-0 d-flex  forum-page-item-links'>
                    <a href="" class='content-link mr-2'> &laquo; <span class='content-link-underline'>предыдущая тема</span>
                    </a>
                    <a href="" class='content-link'><span class='content-link-underline'>следующая тема</span> &raquo;</a>
                </div>
            </nav>
        </section>
    
	<!-- /section -->

<?php get_footer(); ?>
