<?php // Template Name: Page forum-list ?>

<?php get_header(); ?>

                 <?php include_once 'E:\OpenServer\domains\im-tennis.urich.org\wp-content\plugins\bbpress\includes\forums\template.php';?>
                 <?php include_once 'E:\OpenServer\domains\im-tennis.urich.org\wp-content\plugins\bbpress\includes\forums\functions.php';?>
                 <?php include_once 'E:\OpenServer\domains\im-tennis.urich.org\wp-content\plugins\bbpress\includes\core\template-functions.php';?>

<div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="http://im-tennis.urich.org/shop/">Магазин</a></li>
                <li class="breadcrumb-item"><a href="http://im-tennis.urich.org/forum/">Форум</a></li>
                <li class="breadcrumb-item active" aria-current="page"> Инвентарь</li>
            </ol>
        </nav>
        <h2 class="header-section">ФОРУМ</h2>
        <section class="forum-page-item">
            <div class='d-flex justify-content-between align-items-center flex-wrap'>
                <h4 class='forum-item-header'><?php bbp_topic_forum_title("120") ?></h4>                
                
                <form action="" class='forum-page-item-form col-xl-6 col-lg-6 col-md-8 col-sm-12 col-12 p-0'>
                    <label class='form-label  mb-0 mr-2'>Перейти к</label>
                    <select class="form-control col-xl-7 col-lg-7 col-md-7 col-sm-7 col-8 mr-2"  id="exampleFormControlSelect1"> 
                    <?php 
                        $subforum_ids = bbp_get_all_child_ids( '97', bbp_get_forum_post_type() );
                        foreach($subforum_ids as $subforum_item) {
                    ?>
                        <option value="<?= bbp_forum_permalink($subforum_item); ?>"><?php bbp_forum_title($subforum_item);?></option>                        
                    <?php
                        }
                    ?>
                    </select>
                    
                    <a id="select-href" href="<?php echo bbp_forum_permalink($subforum_ids[0]) ?>" class='content-btn'> <img src="E:\OpenServer\domains\im-tennis.urich.org\wp-content\themes\IM-TENNIS\img\search.png" alt="arrow"> </a>
                </form>
            </div>
<?php
                        $forum_id = array("101","103");
                        $topic_ids = bbp_forum_query_topic_ids( "101" );
                        foreach ($topic_ids as $topic_item){
                     ?>
                        <ul class="forum-page-item-list list-unstyled">
                          <li class="media align-items-center mb-3 flex-wrap">
                            <div style='background-image: url(img/forum.png)' class='media-img '></div>
                             <div class="media-body">
                                <a href='http://im-tennis.urich.org/forum-theme/'>
                                   <div class="row-title">
                                     <a class="mt-0 mb-1 media-body-header" href="<?php bbp_topic_permalink($topic_item); ?>"><?php bbp_topic_title($topic_item); ?></a>

                        <?php if (have_posts()): while (have_posts()) : the_post(); ?> 
                            <?php while ( bbp_topics() ) : bbp_the_topic(); ?>
                              <?php bbp_get_template_part( 'loop', 'single-topic' ); ?>
                            <?php endwhile; ?>

                            <?php echo substr($post->post_content,0, 300); ?>
                          

                        <?php endwhile; endif; ?>                 
                                    </div>
                                </a>
                                    <div class='media-body-text'>
                                        Автор <a href='' class='content-link d-inline'><?php bbp_topic_author($topic_item)?></a> в "<a href='' class='content-link d-inline'><?php   ?></a>"
                                    </div>
                            </div>
                                        <div class='media-body-text forum-page-item-list-text col-xl-3 col-lg-3 col-md-4 col-sm-12 col-12'>
                                          <span class='d-block'><?php bbp_topic_post_count($topic_item) ?>  Ответов</span>
                                          <span><?php bbp_topic_reply_count($topic_item) ?> Просмотров</span>
                                        </div>
                                    <div class='media-body-text '>
                                        <div class='d-flex align-items-center'>
                                            <div class='forum-list-item-arrow d-flex align-items-center justify-content-center mr-2'>&rsaquo;</div>
                                                <span class='media-body-text-date '> <?php bbp_topic_post_date($topic_item) ?></span>
                                        </div>
                                                <span>от <a href='' class='content-link d-inline'><?php bbp_topic_author_display_name($topic_item) ?></a></span>
                                    </div>
                         </li>
                            <?php
                            }
                            ?>
                    </ul>
            <nav aria-label="Page navigation" class=''>
                <ul class="pagination justify-content-center">
                    <li class="page-item">
                        <a class="page-link page-link-arrow" href="#" aria-label="Previous">
                            <span aria-hidden="true" class=''>&laquo;</span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">4</a></li>
                    <li class="page-item"><a class="page-link" href="#">5</a></li>
                    <li class="page-item">
                        <a class="page-link page-link-arrow" href="#" aria-label="Next">
                            <span aria-hidden="true" class=''>&raquo;</span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </section>
    </div>
    
	<!-- /section -->






<?php get_footer(); ?>
