//not finished code
// $(".woocommerce-input-wrapper").addClass(function() {
//   $(this).addClass("form-control mb-2");
// });

var product_ids = [];
var divs = document.querySelectorAll(".get_prod_id_class");
for (var i = 0; i < divs.length; i++) {
  product_ids[i] = divs[i].id;
}

//console.log(product_ids);
$(".form-control.mb-2.delivery_sity").change(function() {
  var delivery_nova_poshta = $(
    ".form-control.mb-2.delivery_sity option:selected"
  ).text();

  $("#number_otd_NP").css("display", "block");
});

$(".form-control.mb-2.delivery").change(function() {
  if (
    $(".form-control.mb-2.delivery option:selected").text() == "Новая почта"
  ) {
    $(".form-control.mb-2.delivery_sity").css("display", "block");
    $("#number_index").css("display", "none");
    $("#address_delivery").css("display", "none");
    $("#billing_city").css("display", "none");
  } else if (
    $(".form-control.mb-2.delivery option:selected").text() == "УкрПочта"
  ) {
    $("#number_index").css("display", "block");
    $("#address_delivery").css("display", "block");
    $(".form-control.mb-2.delivery_sity").css("display", "none");
    $("#number_otd_NP").css("display", "none");
  } else {
    $(".form-control.mb-2.delivery_sity").css("display", "none");
    $("#number_index").css("display", "none");
    $("#address_delivery").css("display", "none");
    $("#number_otd_NP").css("display", "none");
    $("#billing_city").css("display", "block");
  }
});

$(".content-btn.w-100.d-block").click(function() {
  // var product_ids = $("#get_product_id").val();
  var fio_cust = $("#billing_first_name").val();
  var email_cust = $("#billing_email").val();
  var tel_cust = $("#billing_phone").val();
  var city_cust = $("#billing_city").val();
  var delivery_cust = $(".form-control.mb-2.delivery option:selected").text();
  var paymant_cust = $(".form-control.mb-2.payment option:selected").text();
  var comment_cust = $("#exampleFormControlTextarea1").val();

  var city_nova_poshta = $(
    ".form-control.mb-2.delivery_sity option:selected"
  ).text();
  var number_otd_NP = $("#exampleFormControlSelect4").val();
  var number_index = $("#exampleFormControlSelect5").val();
  var address_delivery = $("#exampleFormControlSelect6").val();

  if (fio_cust && email_cust && tel_cust) {
    $("#success_send").css("display", "flex");
    $("#Error_empty_data").css("display", "none");

    getAjaxFormCheckout(
      product_ids,
      fio_cust,
      email_cust,
      tel_cust,
      city_cust,
      delivery_cust,
      paymant_cust,
      comment_cust,
      city_nova_poshta,
      number_otd_NP,
      number_index,
      address_delivery
    );
  } else {
    $("#Error_empty_data").css("display", "flex");
    $("#success_send").css("display", "none");
  }
});

function getAjaxFormCheckout(
  array_id_prod,
  fio_cust,
  email,
  tel,
  city,
  delivery,
  paymant,
  comment,
  city_nova_posh,
  number_otd_NP,
  number_index,
  address_delivery
) {
  if (!array_id_prod && !fio_cust && !email) {
    return false;
  } else {
    $.ajax({
      url: myajax.ajax_url,
      type: "post",
      data: {
        id_product: array_id_prod,
        fio_cust: fio_cust,
        email: email,
        tel: tel,
        city: city,
        delivery: delivery,
        paymant: paymant,
        comment: comment,
        city_nova_posh: city_nova_posh,
        number_otd_NP: number_otd_NP,
        number_index: number_index,
        address_delivery: address_delivery,
        _wpnonce: myajax.ajax_nonce,
        action: "get_info_form_checkout"
      },
      success: function(data) {
        $(".test_test").html(data);
        //  console.log(data);
      }
    });
  }
}

function filterMode(obj, mode) {
  if ($(obj).hasClass("cust-no-active")) {
    $(".products-filter-mode").toggleClass("cust-no-active");
    custCreateCookie("prod-type-view", mode, 30);
    window.location.reload(true);
  }
}

function custCreateCookie(name, value, days) {
  var expires;
  if (days) {
    var date = new Date();
    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
    expires = "; expires=" + date.toGMTString();
  } else {
    expires = "";
  }
  document.cookie = name + "=" + value + expires + "; path=/";
}

function sendAjaxForm(cust) {
  if (!cust) return false;

  $.ajax({
    url: myajax.ajax_url,
    type: "post",
    data: {
      cust: cust,
      _wpnonce: myajax.ajax_nonce,
      action: "bild_prod_view"
    },
    success: function(data) {
      // $('#cust_ajax_block').html(data);
      // console.log(data);
    }
  });
}

$(function() {
  if ($(".slick-slider").length > 0) {
    $(".slick-slider").slick({
      infinite: true,
      slidesToShow: 4,
      slidesToScroll: 3,
      prevArrow:
        '<button class="slick-prev"><span class="icon-arrow_left btn-slider"></span></button>',
      nextArrow:
        '<button class="slick-next"><span class="icon-arrow_right btn-slider"></span></button>',
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 577,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });
  }
});

$(function() {
  if ($(".slick-slider-card").length > 0) {
    $(".slider-for").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      asNavFor: ".slick-slider-card"
    });
    $(".slick-slider-card").slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      asNavFor: ".slider-for",
      focusOnSelect: true,
      prevArrow:
        '<button class="slick-prev"><span class="icon-arrow_left btn-slider"></span></button>',
      nextArrow:
        '<button class="slick-next"><span class="icon-arrow_right btn-slider"></span></button>',
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 776,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 321,
          settings: {
            slidesToShow: 2,
            arrows: false
          }
        }
      ]
    });
  }
});
var alertSuccess = $(".woocommerce-notices-wrapper");

alertSuccess.css("display", "none");

$(document).ready(function() {
  var paramsString = location.search;
  var searchParams = new URLSearchParams(paramsString);
  //Iterate the search parameters.
  for (let p of searchParams) {
    //  console.log(p);
  }
  //Сортировка select
  $("#sort").change(function() {
    var sortValues = $("#sort option:selected").val(); //получаем значение выбранного пункта select

    if ($("#sort").val() == "") {
      searchParams.delete("select");
      window.history.replaceState(
        {},
        "",
        location.pathname + "?" + searchParams
      );
      location.reload();
    } else {
      searchParams.set("select", sortValues);
      window.history.replaceState(
        {},
        "",
        location.pathname + "?" + searchParams
      );
      location.reload();
    }
  });
});

jQuery(function($) {
  $("#filter").submit(function() {
    var filter = $(this);
    $.ajax({
      url: myajax.ajax_url, // обработчик
      data: filter.serialize(), // данные
      type: filter.attr("method"), // тип запроса
      beforeSend: function(xhr) {
        filter.find("button").text("Загружаю..."); // изменяем текст кнопки
      },
      success: function(data) {
        filter.find("button").text("Применить фильтр"); // возвращаеи текст кнопки
        $("#response").html(data);
      }
    });
    return false;
  });
});
$(".checkbox_check").click(function() {
  // console.log($(".checkbox_check").prop("checked"));
  $("#btn_filter33").trigger("click");
});

$(".content-btn.w-100.d-block").click(function() {
  setTimeout(hiddenBlock, 605);
  setTimeout(hiddenBlock, 705);
  setTimeout(hiddenBlock, 805);
  setTimeout(hiddenBlock, 905);
  setTimeout(hiddenBlock, 1005);
});

function hiddenBlock() {
  var error_mess = $(".woocommerce-error");
  error_mess.css("display", "none");
}
// ****************************************************************
$(document).ready(function() {
  $("#update_card_qty").prop("disabled", false);
});

function unDisable() {
  // $(".input-text.qty.text").on("click", function(event) {
  $(':input[type="submit"]').prop("disabled", false);
  var valueInput = $(".input-text.qty.text").val();
  //console.log(valueInput);

  var upd_cart_btn = $("#update_card_qty");

  setTimeout(function() {
    upd_cart_btn.trigger("click");
  }, 1000);
}

function checkFilter(event) {
  var input = document.getElementById(`${event.target.name}`);
  //console.log(input);
  var inputId = input.id;
  var input_name = input.name;
  if (!input.hasAttribute("checked")) {
    input.setAttribute("checked", true);
    var block_filter = document.getElementById("filter_span_parent");
    block_filter.innerHTML += `<span style="" id="block-${inputId}" class="fiter-tab ">${input_name}<span onClick="removeBlock(${inputId})" class="fiter-tab-close ml-2"></span></span>`;
  } else {
    input.removeAttribute("checked");
    var span = $(`#block-${inputId}`);
    span.remove();
  }
}

function removeBlock(param) {
  //По нажатию на крестик в спане
  //находим спан и удаляем его
  if (param.id) {
    var span = $(`#block-${param.id}`);
    span.remove();
    //находим соответствующий чекбок спана и отключаем его
    var id_check = param.id;
    document.getElementById(`${id_check}`).removeAttribute("checked");
  } else {
    var span = $(`#block-${param}`);
    span.remove();
    //находим соответствующий чекбок спана и отключаем его
    var id_check = param;
    document.getElementById(`${id_check}`).removeAttribute("checked");
  }
}

document.addEventListener("DOMContentLoaded", function() {
  var aHref = document.querySelectorAll(".kcidJS");
  var divCollapse = document.querySelectorAll(".panel-collapse.collapse.show");
  for (let i = 0; i < aHref.length - 1; i++) {
    aHref[i + 1].setAttribute("href", `#collapse-${i}`);
    divCollapse[i].setAttribute("id", `collapse-${i}`);
  }
});
$(document).ready(function() {
  var image = $(
    ".slick-slider-slide.slick-slide.slick-current.slick-active img"
  );
  image.removeAttr("sizes");
  // console.log(image);
});

//$(document).ready(function() {

$(".content-btn.w-100.d-block").click(function(e) {
  var input_val = document.querySelector(".cart-card-input").value;
  var input_prod_id = $(".cart-card-input").attr("id");
  e.preventDefault();
  //console.log(input_val);
  ajaxAddCart(input_val, input_prod_id);
  setTimeout(function() {
    document.location.reload(true);
  }, 500);
});

function ajaxAddCart(val, id) {
  if (!val) {
    return false;
  } else {
    $.ajax({
      url: myajax.ajax_url,
      type: "post",
      data: {
        id_product: id,
        value_product: val,
        _wpnonce: myajax.ajax_nonce,
        action: "add_cart_to_korzina"
      },
      success: function(data) {
        console.log(data);
      }
    });
  }
}

function trach_reply(id_replys) {
  if (!id_replys) return false;
  else {
    $.ajax({
      url: myajax.ajax_url,
      type: "post",
      data: {
        id_reply: id_replys,
        _wpnonce: myajax.ajax_nonce,
        action: "trach_reply_forum"
      },
      success: function(data) {
        console.log(data);
        document.location.reload(true);
      }
    });
  }
}
//});
