<?php
/*
 *  Author: Ivan
 */

define('THEME_OPT', 'mytheme', true);
/*------------------------------------*\
	Theme Support
\*------------------------------------*/

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail

    // Localisation Support
    // load_theme_textdomain(THEME_OPT, get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/


// Load scripts
function lwp_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

        wp_register_script('themescripts', get_template_directory_uri() . '/assets/js/scripts.js', array('jquery'), '1.0.0'); // Custom scripts
        wp_enqueue_script('themescripts');
    }
}
function tennis_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

        wp_register_script('my_bootstrap', get_template_directory_uri() . '/inc/urich/js/bootstrap.min.js', array('jquery'), '4.1.3',true);
        wp_enqueue_script('my_bootstrap');
        wp_register_script('my_jquery', get_template_directory_uri() . '/inc/urich/js/jquery.min.js', array('jquery'), '3.3.1',false);
        wp_enqueue_script('my_jquery');
        wp_register_script('my_scripts', get_template_directory_uri() . '/inc/urich/js/scripts.min.js', array('jquery'), false,true); // Custom scripts
        wp_enqueue_script('my_scripts');
        wp_register_script('my_slick', get_template_directory_uri() . '/inc/urich/js/slick.js', array('jquery'), 'false',true);
        wp_enqueue_script('my_slick');
        wp_register_script('my_common', get_template_directory_uri() . '/inc/urich/js/common.js', array('jquery'), false,true);
		wp_enqueue_script('my_common');
		wp_register_script('filter_shop', get_template_directory_uri() . '/inc/urich/js/filter.js', array('jquery'), false,true);
        wp_enqueue_script('filter_shop');
        // wp_register_script('my-js', get_template_directory_uri() . '/inc/urich/js/my-js.js', array('jquery'), 'false',true);
        // wp_enqueue_script('my-js');        
    }
}

add_action( 'wp_enqueue_scripts', 'myajax_data', 99 );
function myajax_data(){
	wp_localize_script('my_common', 'myajax', 
		array(
			'ajax_url' => admin_url('admin-ajax.php'),
			'ajax_nonce' => wp_create_nonce('myajax-nonce')
		)
	);  
}

add_action( 'wp_ajax_bild_prod_view', 'bild_product_page' );
add_action('wp_ajax_nopriv_bild_prod_view', 'bild_product_page');

function bild_product_page() {
    get_template_part( 'templates/loop-products', null );

wp_die();
}

// Load styles
function lwp_styles() {

    wp_register_style('themestyle', get_template_directory_uri() . '/assets/css/style.css', array(), filemtime(get_template_directory() . '/assets/css/style.css'), 'all');
    wp_enqueue_style('themestyle');
}
function tennis_styles() {

    wp_register_style('tennis_style', get_template_directory_uri() . '/inc/urich/css/styles.min.css');
    wp_enqueue_style('tennis_style');
    wp_register_style('bootstrap_style', get_template_directory_uri() . '/inc/urich/css/bootstrap.min.css');
    wp_enqueue_style('bootstrap_style');
    wp_register_style('bootstrap_grid_style', get_template_directory_uri() . '/inc/urich/css/bootstrap-grid.min.css');
    wp_enqueue_style('bootstrap_grid_style');
    wp_register_style('bootstrap-reboot_style', get_template_directory_uri() . '/inc/urich/css/bootstrap-reboot.min.css');
    wp_enqueue_style('bootstrap-reboot_style');
    wp_register_style('icomoon_style', get_template_directory_uri() . '/inc/urich/css/icomoon.css');
    wp_enqueue_style('icomoon_style');

}

// HTML5 Blank navigation
function header_nav()
{
	wp_nav_menu(
	array(
		'theme_location'  => 'header-menu',
		'menu'            => '',
		'container'       => '',
		'container_class' => '',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul class="d-flex align-items-center justify-content-between flex-wrap mb-0 header-menu-list">%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
		)
	);
}
function subheader_nav()
{
    wp_nav_menu(
        array(
            'theme_location'  => 'subheader-menu',
            'menu'            => '',
            'container'       => 'div',
            'container_class' => 'container',
            'container_id'    => '',
            'menu_class'      => 'menu',
            'menu_id'         => '',
            'echo'            => true,
            'fallback_cb'     => 'wp_page_menu',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'items_wrap'      => '<ul class="d-flex justify-content-xl-start justify-content-lg-start justify-content-center flex-wrap mb-0 subheader-list">%3$s</ul>',
            'depth'           => 0,
            'walker'          => ''
        )
    );
}
add_filter( 'nav_menu_link_attributes', 'add_my_class_to_links_menu', 10, 3 );

function add_my_class_to_links_menu( $atts, $item, $args ) {
    if( $args->theme_location == 'header-menu' ) {
        $atts['class'] = 'header-menu-list-link';
    }
    elseif ($args->theme_location == 'subheader-menu') {
        $atts['class'] = 'subheader-list-link';
    }
    return $atts;
}

// Register Navigation
function register_lwp_menu()
{
    register_nav_menus(array(
        'header-menu' => __('Header Menu', THEME_OPT),
        'subheader-menu' => __('Under Header Menu', THEME_OPT),
        'footer-menu' => __('Footer Menu', THEME_OPT),
    ));
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    // Define Sidebar Widget Area 1
    register_sidebar(array(
		'name' => __('Widget Area 1', 'teatrhotel'),
        'description' => __('Description for this widget-area...', THEME_OPT),
        'id' => 'widget-area-1',
        'before_widget' => '<ul class="off-canvas-list panel-group">',
        'after_widget' => '</ul>',
		'before_title' => '<div class="panel panel-default"><div class="panel-heading">
		<h4 class="panel-title-header d-flex justify-content-between align-items-center"><a data-toggle="collapse" href="#collapse-" class="panel-title-header d-flex justify-content-between align-items-center kcidJS" style="width: 100%" aria-expanded="true">',
		
        'after_title' => '<span class="filter-arrow"></span></a></h3></div></div>'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function lwp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

// Custom Excerpts
function lwp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function lwp_custom_post($length)
{
    return 40;
}

// Create the Custom Excerpts callback
function lwp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Remove Admin bar
function remove_admin_bar()
{
    return false;
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}

// Custom Comments Callback
function lwpcomments($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
	<div class="comment-author vcard">
	<?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
	<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
	</div>
<?php if ($comment->comment_approved == '0') : ?>
	<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
	<br />
<?php endif; ?>

	<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
		<?php
			printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
		?>
	</div>

	<?php comment_text() ?>

	<div class="reply">
	<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	</div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif; ?>
<?php }

/*------------------------------------*\
	Actions + Filters
\*------------------------------------*/

// Add Actions
add_action('wp_enqueue_scripts', 'lwp_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_enqueue_scripts', 'tennis_scripts'); // Add TENNIS Scripts to wp_head
add_action('wp_enqueue_scripts', 'lwp_styles'); // Add Theme Stylesheet
add_action('wp_enqueue_scripts', 'tennis_styles'); // Add Theme Stylesheet
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('init', 'register_lwp_menu'); // Add HTML5 Blank Menu
add_action('init', 'lwp_pagination'); // Add our HTML5 Pagination
 
// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

// Reduxframework
include_once 'inc/loader.php';
require_once 'inc/helpers.php';

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}
function be_display_custom_posts($query)
{
//    query_posts([
//        'posts_per_page' => -1,
//        'meta_query' => array(
//                'key' => 'price',
//                'value' => 0,
//                //'type' => 'numeric',
//                'compare' => '>',
//                'post_type' => 'product',
//                'post_status' => 'publish',
//                'columns' => '4',
//                'meta_key' => 'total_sales',
//                'meta_query' => WC()->query->get_meta_query(),
//                'limit' => 10,
//                'orderby' => 'date',
//                'order' => 'DESC',
//                'product_tag' => 'Новинка'
//        )
//    ]);

//    if ($query->is_main_query() && !$query->is_feed() && !is_admin() && $query->is_post_type_archive('event')) {
////        $meta_query = array(
////            array(
////                'key' => 'be_events_manager_end_date',
////                'value' => time(),
////                'compare' => '>'
////            )
////        );
////        $query->set('meta_query', $meta_query);
//        $query->set('orderby', 'meta_value_num');
//        $query->set('meta_key', 'be_events_manager_start_date');
//        $query->set('order', 'ASC');
//        $query->set('posts_per_page', '4');
//    }
//    https://wordpress.stackexchange.com/questions/221760/how-do-i-query-for-posts-by-partial-meta-key
//    https://stackoverflow.com/questions/24216915/pagination-in-woocommerce
//    https://www.billerickson.net/customize-the-wordpress-query/
}


add_filter( 'woocommerce_form_field', 'filter_function_name_3927', 10, 4 );
function filter_function_name_3927( $field, $key, $args, $value ){
	$defaults = array(
		'type'              => 'text',
		'label'             => '',
		'description'       => '',
		'placeholder'       => '',
		'maxlength'         => false,
		'required'          => false,
		'autocomplete'      => false,
		'id'                => $key,
		'class'             => array(),
		'label_class'       => array(),
		'input_class'       => array(),
		'return'            => false,
		'options'           => array(),
		'custom_attributes' => array(),
		'validate'          => array(),
		'default'           => '',
		'autofocus'         => '',
		'priority'          => '',
	);

	$args = wp_parse_args( $args, $defaults );
	$args = apply_filters( 'woocommerce_form_field_args', $args, $key, $value );

	if ( $args['required'] ) {
		$args['class'][] = 'validate-required';
		$required        = '&nbsp;<abbr class="required" title="' . esc_attr__( 'required', 'woocommerce' ) . '">*</abbr>';
	} else {
		$required = '&nbsp;<span class="optional">' . esc_html__( '', 'woocommerce' ) . '</span>';
	}

	if ( is_string( $args['label_class'] ) ) {
		$args['label_class'] = array( $args['label_class'] );
	}

	if ( is_null( $value ) ) {
		$value = $args['default'];
	}

	// Custom attribute handling.
	$custom_attributes         = array();
	$args['custom_attributes'] = array_filter( (array) $args['custom_attributes'], 'strlen' );

	if ( $args['maxlength'] ) {
		$args['custom_attributes']['maxlength'] = absint( $args['maxlength'] );
	}

	if ( ! empty( $args['autocomplete'] ) ) {
		$args['custom_attributes']['autocomplete'] = $args['autocomplete'];
	}

	if ( true === $args['autofocus'] ) {
		$args['custom_attributes']['autofocus'] = 'autofocus';
	}

	if ( $args['description'] ) {
		$args['custom_attributes']['aria-describedby'] = $args['id'] . '-description';
	}

	if ( ! empty( $args['custom_attributes'] ) && is_array( $args['custom_attributes'] ) ) {
		foreach ( $args['custom_attributes'] as $attribute => $attribute_value ) {
			$custom_attributes[] = esc_attr( $attribute ) . '="' . esc_attr( $attribute_value ) . '"';
		}
	}

	if ( ! empty( $args['validate'] ) ) {
		foreach ( $args['validate'] as $validate ) {
			$args['class'][] = 'validate-' . $validate;
		}
	}

	$field           = '';
	$label_id        = $args['id'];
	$sort            = $args['priority'] ? $args['priority'] : '';
	$field_container = '<p class="form-row %1$s" id="%2$s" data-priority="' . esc_attr( $sort ) . '">%3$s</p>';

	switch ( $args['type'] ) {
		case 'country':
		$field .= '<input style="border-style: none;" type="text" class="input-text ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" placeholder="' . esc_attr( $args['placeholder'] ) . '"  value="Украина" readonly ' . implode( ' ', $custom_attributes ) . ' />';
			break;
		case 'state':
			/* Get country this state field is representing */
			$for_country = isset( $args['country'] ) ? $args['country'] : WC()->checkout->get_value( 'billing_state' === $key ? 'billing_country' : 'shipping_country' );
			$states      = WC()->countries->get_states( $for_country );

			if ( is_array( $states ) && empty( $states ) ) {

				$field_container = '<p class="form-row %1$s" id="%2$s" style="display: none">%3$s</p>';

				$field .= '<input type="hidden" class="hidden" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" value="" ' . implode( ' ', $custom_attributes ) . ' placeholder="' . esc_attr( $args['placeholder'] ) . '" readonly="readonly" />';

			} elseif ( ! is_null( $for_country ) && is_array( $states ) ) {

				$field .= '<select name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" class="state_select ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" ' . implode( ' ', $custom_attributes ) . ' data-placeholder="' . esc_attr( $args['placeholder'] ? $args['placeholder'] : esc_html__( 'Select an option&hellip;', 'woocommerce' ) ) . '">
					<option value="">' . esc_html__( 'Select an option&hellip;', 'woocommerce' ) . '</option>';

				foreach ( $states as $ckey => $cvalue ) {
					$field .= '<option value="' . esc_attr( $ckey ) . '" ' . selected( $value, $ckey, false ) . '>' . $cvalue . '</option>';
				}

				$field .= '</select>';

			} else {

				$field .= '<input style="border-style: none;" type="text" class="input-text ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" value="' . esc_attr( $value ) . '"  placeholder="' . esc_attr( $args['placeholder'] ) . '" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" ' . implode( ' ', $custom_attributes ) . ' />';

			}

			break;
		case 'textarea':
			$field .= '<textarea name="' . esc_attr( $key ) . '" class="input-text ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" id="' . esc_attr( $args['id'] ) . '" placeholder="' . esc_attr( $args['placeholder'] ) . '" ' . ( empty( $args['custom_attributes']['rows'] ) ? ' rows="2"' : '' ) . ( empty( $args['custom_attributes']['cols'] ) ? ' cols="5"' : '' ) . implode( ' ', $custom_attributes ) . '>' . esc_textarea( $value ) . '</textarea>';

			break;
		case 'checkbox':
			$field = '<label class="checkbox ' . implode( ' ', $args['label_class'] ) . '" ' . implode( ' ', $custom_attributes ) . '>
					<input type="' . esc_attr( $args['type'] ) . '" class="input-checkbox ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" value="1" ' . checked( $value, 1, false ) . ' /> ' . $args['label'] . $required . '</label>';

			break;
		case 'text':
		case 'password':
		case 'datetime':
		case 'datetime-local':
		case 'date':
		case 'month':
		case 'time':
		case 'week':
		case 'number':
		case 'email':
		case 'url':
		case 'tel':
			$field .= '<input style="border-style: none;" type="' . esc_attr( $args['type'] ) . '" class="input-text ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" placeholder="' . esc_attr( $args['placeholder'] ) . '"  value="' . esc_attr( $value ) . '" ' . implode( ' ', $custom_attributes ) . ' />';

			break;
		case 'select':
			$field   = '';
			$options = '';
			break;
		case 'radio':
			$label_id .= '_' . current( array_keys( $args['options'] ) );

			if ( ! empty( $args['options'] ) ) {
				foreach ( $args['options'] as $option_key => $option_text ) {
					$field .= '<input type="radio" class="input-radio ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" value="' . esc_attr( $option_key ) . '" name="' . esc_attr( $key ) . '" ' . implode( ' ', $custom_attributes ) . ' id="' . esc_attr( $args['id'] ) . '_' . esc_attr( $option_key ) . '"' . checked( $value, $option_key, false ) . ' />';
					$field .= '<label for="' . esc_attr( $args['id'] ) . '_' . esc_attr( $option_key ) . '" class="radio ' . implode( ' ', $args['label_class'] ) . '">' . $option_text . '</label>';
				}
			}

			break;
	}
	if ( ! empty( $field ) ) {
		$field_html = '';

		if ( $args['label'] && 'checkbox' !== $args['type'] ) {
			$field_html .= '<label for="' . esc_attr( $label_id ) . '" class="' . esc_attr( implode( ' ', $args['label_class'] ) ) . '">' . $args['label'] . $required . '</label>';
		}

		$field_html .= '<span class="woocommerce-input-wrapper form-control mb-2">' . $field;

		if ( $args['description'] ) {
			$field_html .= '<span class="description" id="' . esc_attr( $args['id'] ) . '-description" aria-hidden="true">' . wp_kses_post( $args['description'] ) . '</span>';
		}

		$field_html .= '</span>';

		$container_class = esc_attr( implode( ' ', $args['class'] ) );
		$container_id    = esc_attr( $args['id'] ) . '_field';
		$field           = sprintf( $field_container, $container_class, $container_id, $field_html );
	}
		if ( $args['return'] ) {
			return $field;
		} else {
			echo $field; // WPCS: XSS ok.
		}
}



function woocommerce_set_cart_qty_action() { 
    global $woocommerce;
    foreach ($_REQUEST as $key => $quantity) {
    // only allow integer quantities
    if (! is_numeric($quantity)) continue;

        // attempt to extract product ID from query string key
        $update_directive_bits = preg_split('/^set-cart-qty_/', $key);
        if (count($update_directive_bits) >= 2 and is_numeric($update_directive_bits[1])) {
            $product_id = (int) $update_directive_bits[1]; 
            $cart_id = $woocommerce->cart->generate_cart_id($product_id);
            // See if this product and its options is already in the cart
            $cart_item_key = $woocommerce->cart->find_product_in_cart( $cart_id ); 
            // If cart_item_key is set, the item is already in the cart
            if ( $cart_item_key ) {
                $woocommerce->cart->set_quantity($cart_item_key, $quantity);
            } else {
                // Add the product to the cart 
                $woocommerce->cart->add_to_cart($product_id, $quantity);
            }
        }
    }
}


function redirect_to_checkout() {
global $woocommerce;
$checkout_url = $woocommerce->cart->get_checkout_url();
return $checkout_url;
}

add_action( 'genesis_after_entry', 'be_display_custom_posts' );

?>

<?php 

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
 
function custom_override_checkout_fields( $fields ) {

    unset($fields['billing']['billing_last_name']);
unset($fields['billing']['billing_company']);
unset($fields['billing']['billing_address_1']);
unset($fields['billing']['billing_address_2']);
unset($fields['billing']['billing_postcode']);
unset($fields['order']['order_comments']);
unset($fields['account']['account_username']);
unset($fields['account']['account_password']);
unset($fields['account']['account_password-2']);

 unset($fields['billing']['billing_state']);

$fields['billing']['billing_first_name']['label']='Личные данные';
$fields['billing']['billing_first_name']['span']='style="display:none"';
    
    unset($fields['order']['order_comments']);
    return $fields;
}

add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 4;' ), 20 );

function register_my_widgets_filter(){
	register_sidebar( array(
		'name' => 'YITH WooCommerce Ajax Reset Filter sidebar',
		'id' => 'yith_woocommerce_ajax_product_filter',
		'description' => '',
		'before_widget' => '<li class="filte_class">',
		'after_widget' => '</li>',
		'before_title' => '<h2 class="widgettitle_filter">',
		'after_title' => '</h2>',
	) );
}
  
add_action( 'widgets_init', 'register_my_widgets_filter');

add_filter ('loop_shop_per_page', 'new_loop_shop_per_page', 20);

function new_loop_shop_per_page ($cols) {
  // $ cols содержит текущее количество товаров на странице в зависимости от значения, сохраненного в Options -> Reading
  // Возвращаем количество товаров, которые вы хотите показать на странице.
  if(isset($_COOKIE['prod-type-view']) && $_COOKIE['prod-type-view'] == 'list'){
	$cols = 15;
}   
 else
  $cols = 9;
  return $cols;
}

class trueTopPostsWidget extends WP_Widget {
 
	/*
	 * создание виджета
	 */
	function __construct() {
		parent::__construct(
			'true_top_widget', 
			'Фильтр товаров', // заголовок виджета
			array( 'description' => 'Позволяет фильтровать товары по категориям' ) // описание
		);
	}
 
	/*
	 * фронтэнд виджета
	 */
	public function widget($args='', $instance='') {
        ?> 
		<!-- Форма фильтра товара -->
		<form action="<?php echo site_url() ?>/wp-admin/admin-ajax.php" method="POST" class='filter-form' id="filter">
					<div class="col-12 ">
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title panel-title-header">
										Цена
									</h4>
								</div>
								<div class="panel-body d-flex align-items-center justify-content-between filter-price mb-3">
									От
									<input id="hidden_minimum_price" type="number" min="0" max="80000" value='0' name="cena_min" class='filter-price-input'>
									до
									<input id="hidden_maximum_price" type="number" min="0" max="80000" value='80000' name="cena_max" class='filter-price-input'>
									грн
								</div>
								<p id="price_show">1000 - 65000</p>
							</div>
						</div>
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title ">
										<a data-toggle="collapse" href="#collapse1" class="panel-title-header d-flex justify-content-between align-items-center collapsed">Производитель<span
											 class='filter-arrow'></span></a>
									</h4>
								</div>
								<div id="collapse1" class="panel-collapse collapse">
									<div class="panel-body">
										<ul class='mb-3'>
											<li>
												<div class="checkbox">
													<input class="checkbox_check brand" type="checkbox" name="check_1" value="" id="check_1" class="filter_checkbox ">
													<label for="check_1" class='d-flex align-items-center'>
														999</label>
												</div>
											</li>
											<li>
												<div class="checkbox">
													<input class="checkbox_check brand" type="checkbox" name="check_2" value="" id="check_2" class="filter_checkbox">
													<label for="check_2" class='d-flex align-items-center'>
														DHS</label>
												</div>
											</li>
											<li>
												<div class="checkbox">
													<input class="checkbox_check brand" type="checkbox" name="check_3" value="" id="check_3" class="filter_checkbox">
													<label for="check_3" class='d-flex align-items-center'>
                                                    SPINLORD</label>
												</div>
											</li>
											<li>
												<div class="checkbox">
													<input class="checkbox_check brand" type="checkbox" name="check_4" value="" id="check_4" class="filter_checkbox">
													<label for="check_4" class='d-flex align-items-center'>
                                                    YINHE (Milkyway)</label>
												</div>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
                        <div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title ">
										<a data-toggle="collapse" href="#collapse2" class="panel-title-header d-flex justify-content-between align-items-center collapsed">Наличие<span
											 class='filter-arrow'></span></a>
									</h4>
								</div>
								<div id="collapse2" class="panel-collapse collapse">
									<div class="panel-body">
										<ul class='mb-3'>
											<li>
												<div class="checkbox">
													<input class="checkbox_check availability" type="checkbox" name="check_5" value="" id="check_5" class="filter_checkbox ">
													<label for="check_5" class='d-flex align-items-center'>
														Только товары в наличии</label>
												</div>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
                        <div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title ">
										<a data-toggle="collapse" href="#collapse3" class="panel-title-header d-flex justify-content-between align-items-center collapsed">ТИП<span
											 class='filter-arrow'></span></a>
									</h4>
								</div>
								<div id="collapse3" class="panel-collapse collapse">
									<div class="panel-body">
										<ul class='mb-3'>
											<li>
												<div class="checkbox">
													<input class="checkbox_check type" type="checkbox" name="check_6" value="" id="check_6" class="filter_checkbox ">
													<label for="check_6" class='d-flex align-items-center'>
														DEF</label>
												</div>
											</li>
											<li>
												<div class="checkbox">
													<input class="checkbox_check type" type="checkbox" name="check_7" value="" id="check_7" class="filter_checkbox">
													<label for="check_7" class='d-flex align-items-center'>
														DEF+</label>
												</div>
											</li>
											<li>
												<div class="checkbox">
													<input class="checkbox_check type" type="checkbox" name="check_8" value="" id="check_8" class="filter_checkbox">
													<label for="check_8" class='d-flex align-items-center'>
                                                    ALL-</label>
												</div>
											</li>
											<li>
												<div class="checkbox">
													<input class="checkbox_check type" type="checkbox" name="check_9" value="" id="check_9" class="filter_checkbox">
													<label for="check_9" class='d-flex align-items-center'>
                                                    ALL</label>
												</div>
											</li>
                                            <li>
												<div class="checkbox">
													<input class="checkbox_check type" type="checkbox" name="check_10" value="" id="check_10" class="filter_checkbox">
													<label for="check_10" class='d-flex align-items-center'>
                                                    ALL+</label>
												</div>
											</li>
                                            <li>
												<div class="checkbox">
													<input class="checkbox_check type" type="checkbox" name="check_11" value="" id="check_11" class="filter_checkbox">
													<label for="check_11" class='d-flex align-items-center'>
                                                    OFF-</label>
												</div>
											</li>
                                            <li>
												<div class="checkbox">
													<input class="checkbox_check type" type="checkbox" name="check_12" value="" id="check_12" class="filter_checkbox">
													<label for="check_12" class='d-flex align-items-center'>
                                                    OFF</label>
												</div>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title  mb-2">
										<a data-toggle="collapse" href="#collapse4" class="panel-title-header d-flex justify-content-between align-items-center collapsed">ТОЛЩИНА 
                                        НАКЛАДОК
                                        <span class='filter-arrow'></span></a>
									</h4>
								</div>
								<div id="collapse4" class="panel-collapse collapse">
									<div class="panel-body">
										<ul class='mb-3'>
											<li>
												<div class="checkbox">
													<input class="checkbox_check lining" type="checkbox" name="check_13" value="" id="check_13" class="filter_checkbox ">
													<label for="check_13" class='d-flex align-items-center'>
														1.6</label>
												</div>
											</li>
											<li>
												<div class="checkbox">
													<input class="checkbox_check lining" type="checkbox" name="check_14" value="" id="check_14" class="filter_checkbox">
													<label for="check_14" class='d-flex align-items-center'>
														2.0</label>
												</div>
											</li>
											<li>
												<div class="checkbox">
													<input class="checkbox_check lining" type="checkbox" name="check_15" value="" id="check_15" class="filter_checkbox">
													<label for="check_15" class='d-flex align-items-center'>
														2.1</label>
												</div>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
                        <div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title  mb-2">
										<a data-toggle="collapse" href="#collapse5" class="panel-title-header d-flex justify-content-between align-items-center collapsed">СРЕДНИЙ ВЕС
                                        <span class='filter-arrow'></span></a>
									</h4>
								</div>
								<div id="collapse5" class="panel-collapse collapse">
									<div class="panel-body">
										<ul class='mb-3'>
											<li>
												<div class="checkbox">
													<input class="checkbox_check weight" type="checkbox" name="check_16" value="" id="check_16" class="filter_checkbox ">
													<label for="check_16" class='d-flex align-items-center'>
														170</label>
												</div>
											</li>
											<li>
												<div class="checkbox">
													<input class="checkbox_check weight" type="checkbox" name="check_17" value="" id="check_17" class="filter_checkbox">
													<label for="check_17" class='d-flex align-items-center'>
														180</label>
												</div>
											</li>
											<li>
												<div class="checkbox">
													<input class="checkbox_check weight" type="checkbox" name="check_18" value="" id="check_18" class="filter_checkbox">
													<label for="check_18" class='d-flex align-items-center'>
														190</label>
												</div>
											</li>
                                            <li>
												<div class="checkbox">
													<input class="checkbox_check weight" type="checkbox" name="check_19" value="" id="check_19" class="filter_checkbox">
													<label for="check_19" class='d-flex align-items-center'>
														200</label>
												</div>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>

					</div>
                            <button id="btn_filter33" style="opacity:0">Применить фильтр</button>
                                <input type="hidden" name="action" value="myfilter">            
					<div class='d-flex justify-content-center mt-4 mb-4'><input type='submit' class="filter-link" value='сбросить фильтр' /></div>
                </form> 
                <?php
	}
	public function form( $new_instance='', $old_instance='' ) {
	
	}
	public function update($new_instance='', $old_instance='') {
        return $instance;
	}
}
 
/*
 * регистрация виджета
 */
function true_top_posts_widget_load() {
	register_widget( 'trueTopPostsWidget' );
}
add_action( 'widgets_init', 'true_top_posts_widget_load' );

function true_filter_function(){
	
	// создаём массив $args['meta_query'] если указана хотя бы одна цена или отмечен чекбокс
	if( isset( $_POST['cena_min'] ) || isset( $_POST['cena_max'] ))
		$args['meta_query'] = array( 'relation'=>'AND' ); // AND значит все условия meta_query должны выполняться
 
	// условие 1: цена больше $_POST['cena_min']
	if( isset( $_POST['cena_min'] ) )
		$args['meta_query'][] = array(
			'key' => 'cena',
			'value' => $_POST['cena_min'],
			'type' => 'numeric',
			'compare' => '>'
		);
 
	// условие 2: цена меньше $_POST['cena_max']
	if( isset( $_POST['cena_max'] ) )
		$args['meta_query'][] = array(
			'key' => 'cena',
			'value' => $_POST['cena_max'],
			'type' => 'numeric',
			'compare' => '<'
		);
	if( isset( $_POST['check_1'] ))
		$args['meta_query'][] = array(
			'key' => 'check_1',
			'compare' => 'true'
        );
        if( isset( $_POST['check_2'] ))
		$args['meta_query'][] = array(
			'key' => 'check_2',
			'compare' => 'true'
        );
        if( isset( $_POST['check_3'] ))
		$args['meta_query'][] = array(
			'key' => 'check_3',
			'compare' => 'true'
        );
        if( isset( $_POST['check_4'] ))
		$args['meta_query'][] = array(
			'key' => 'check_4',
			'compare' => 'true'
        );
        if( isset( $_POST['check_5'] ))
		$args['meta_query'][] = array(
			'key' => 'check_5',
			'compare' => 'true'
        );
        if( isset( $_POST['check_6'] ))
		$args['meta_query'][] = array(
			'key' => 'check_6',
			'compare' => 'true'
        );
        if( isset( $_POST['check_7'] ))
		$args['meta_query'][] = array(
			'key' => 'check_7',
			'compare' => 'true'
        );
        if( isset( $_POST['check_8'] ))
		$args['meta_query'][] = array(
			'key' => 'check_8',
			'compare' => 'true'
        );
        if( isset( $_POST['check_9'] ))
		$args['meta_query'][] = array(
			'key' => 'check_9',
			'compare' => 'true'
        );
        if( isset( $_POST['check_10'] ))
		$args['meta_query'][] = array(
			'key' => 'check_10',
			'compare' => 'true'
        );
        if( isset( $_POST['check_11'] ))
		$args['meta_query'][] = array(
			'key' => 'check_11',
			'compare' => 'true'
        );
        if( isset( $_POST['check_12'] ))
		$args['meta_query'][] = array(
			'key' => 'check_12',
			'compare' => 'true'
        );
        if( isset( $_POST['check_13'] ))
		$args['meta_query'][] = array(
			'key' => 'check_13',
			'compare' => 'true'
        );
        if( isset( $_POST['check_14'] ))
		$args['meta_query'][] = array(
			'key' => 'check_14',
			'compare' => 'true'
        );
        if( isset( $_POST['check_15'] ))
		$args['meta_query'][] = array(
			'key' => 'check_15',
			'compare' => 'true'
        );
        if( isset( $_POST['check_16'] ))
		$args['meta_query'][] = array(
			'key' => 'check_16',
			'compare' => 'true'
        );
        if( isset( $_POST['check_17'] ))
		$args['meta_query'][] = array(
			'key' => 'check_17',
			'compare' => 'true'
        );
        if( isset( $_POST['check_18'] ))
		$args['meta_query'][] = array(
			'key' => 'check_18',
			'compare' => 'true'
        );
        if( isset( $_POST['check_19'] ))
		$args['meta_query'][] = array(
			'key' => 'check_19',
			'compare' => 'true'
		);
 
	die();
}
 
add_action('wp_ajax_myfilter', 'true_filter_function'); 
add_action('wp_ajax_nopriv_myfilter', 'true_filter_function');


add_action( 'wp_ajax_get_info_form_checkout', 'get_inform_customer_checkout' );
add_action('wp_ajax_nopriv_get_info_form_checkout', 'get_inform_customer_checkout');

function get_inform_customer_checkout() {
	$id_product = $_POST['id_product'];
	$fio_cust = $_POST['fio_cust'];
	$email = $_POST['email'];
	$tel = $_POST['tel'];
	$city = $_POST['city'];
	$delivery = $_POST['delivery'];
	$paymant = $_POST['paymant'];
	$comment = $_POST['comment'];
	$city_nova_posh = $_POST['city_nova_posh'];
	$number_otd_NP = $_POST['number_otd_NP'];
	$number_index = $_POST['number_index'];
	$address_delivery = $_POST['address_delivery'];
	//print_r($id_product);
if($city_nova_posh){
	$city = $city_nova_posh;
}

// 	if(!$id_product){
// 		wp_send_json ( 'Error: Invalid customer ID!', 422);
// 		wp_die();
// 	}

	$order_data                        = array();
	$order_data[ 'post_type' ]         = 'shop_order';
	$order_data[ 'post_status' ]       = 'wc-' . apply_filters( 'woocommerce_default_order_status', 'pending' );
	$order_data[ 'ping_status' ]       = 'closed';
	$order_data[ 'post_author' ]       = 1;
	$order_data[ 'post_password' ]     = uniqid( 'order_' );
	$order_data[ 'post_title' ]        = sprintf( __( 'Order &ndash; %s', 'woocommerce' ), strftime( _x( '%b %d, %Y @ %I:%M %p', 'Order date parsed by strftime', 'woocommerce' ), strtotime( date_default_timezone_set('UTC') ) ) );
	$order_data[ 'post_parent' ]       = 12; // parent post id
	$order_data[ 'post_content' ]      = "";
	$order_data[ 'comment_status' ]    = "open";
	$order_data[ 'post_name' ]         = sanitize_title( sprintf( __( 'Order &ndash; %s', 'woocommerce' ), strftime( _x( '%b %d, %Y @ %I:%M %p', 'Order date parsed by strftime', 'woocommerce' ), strtotime( date_default_timezone_set('UTC')) ) ) );
	
	$order_id = wp_insert_post( apply_filters( 'woocommerce_new_order_data', $order_data ), true );

	$order = wc_get_order( $order_id );
	for($i=0; $i<count($id_product); $i++){
		$product_item_id = $order->add_product( wc_get_product( $id_product[$i] ));
		wc_add_order_item_meta($product_item_id,"_city",$city);
		wc_add_order_item_meta($product_item_id,"_delivery",$delivery);
		wc_add_order_item_meta($product_item_id,"_paymant",$paymant);
		wc_add_order_item_meta($product_item_id,"_comment",$comment);
		wc_add_order_item_meta($product_item_id,"_city_nova_posh",$city_nova_posh);
		wc_add_order_item_meta($product_item_id,"_number_otd_NP",$number_otd_NP);
		wc_add_order_item_meta($product_item_id,"_number_index",$number_index);
		wc_add_order_item_meta($product_item_id,"_address_delivery",$address_delivery);
	}
	//echo $product_item_id;
$addressShipping = array(
		   'first_name' => $fio_cust,
		   'Last name'  => $number_otd_NP,
		'email'      => $email,
		'phone'      => $tel,
		'address_1'  => $address_delivery,
		'city'       => $city,
		'Postcode / ZIP'   => $number_index,
		'country'    => 'Украина',
		'delivery'   => $delivery,
		'paymant'    => $paymant,
		'comment'    => $comment
	);
$order->set_address( $addressShipping, 'shipping' );
    $addressBilling = array(
		'first_name' => $fio_cust,
		'Last name'  => $number_otd_NP,
		'email'      => $email,
		'phone'      => $tel,
		'address_1'  => $address_delivery,
		'city'       => $city,
		'Postcode / ZIP'   => $number_index,
		'country'    => 'Украина',
		'delivery'   => $delivery,
		'paymant'    => $paymant,
		'comment'    => $comment
	);
$order->set_address( $addressBilling, 'billing' );
$order->calculate_totals();
// 	echo '<pre>';// print_r($order);
// 	echo $product_item_id;
// 	echo '</pre>';
// 	 echo'<pre>';
// echo $id_product;
// echo'</pre>';

$headers= "MIME-Version: 1.0\r\n";						
$headers .= "Content-type: text/html; charset=utf-8\r\n";
			$message =  '<hr>
						 <p>Заказ: '. $order->calculate_totals().'</p>
						 <p><br /> </p>
						
						<p>Фамилия имя отчество: '. $fio_cust.' </p>
						<p>email:                '. $email.' </p>
						<p>телефон:              '. $tel.' </p>
						<p>Город:                '. $city.' </p>
						<p>Условия доставки:     '. $delivery.' </p>
						<p>Город Новой почты:    '. $city_nova_posh.' </p>
						<p>Номер отделения:      '. $number_otd_NP.' </p>
						<p>Почтовый индекс:      '. $number_index.' </p>
						<p>Адрес доставки:       '. $address_delivery.' </p>
						<p>Условия оплаты:       '. $paymant.' </p>
						<p>Комментарий:          '. $comment.' </p>
						</hr>';
	mail ('povaliaef@gmail.com', "Заявка на покупку товара с сайта http://im-tennis.urich.org", $message,$headers);



	wp_die();
}




add_action('wp_ajax_fetch_data', 'filter_function_shop'); 
add_action('wp_ajax_nopriv_fetch_data', 'filter_function_shop');

function filter_function_shop(){
$min_price = $_POST['minimum_price'];
$max_price = $_POST['maximum_price'];
$brand = $_POST['brand'];
$availability = $_POST['availability'];
$type = $_POST['type'];
$lining = $_POST['lining'];
$weight = $_POST['weight'];
 echo  $min_price;
// echo  $max_price;
// print_r($brand);
// echo  $availability;
// echo  $type;
// echo  $lining;
// echo  $weight;

	wp_die();
}


function get_attr_by_product($attr_id, $id_sub_attr){
	global $wpdb;
	
	for($i = 0; $i < count($attr_id); $i++){
		$attr_ids[$i] = substr($attr_id[$i], 3);
	}
		for($i = 0; $i < count($attr_id); $i++){
			$query_from1 = "SELECT `attribute_label` FROM `im_woocommerce_attribute_taxonomies` WHERE `attribute_name` = '$attr_ids[$i]'";
			$query_from2 = "SELECT `name` FROM `im_terms` WHERE `term_id` = '$id_sub_attr[$i]'";
			$label_attr = $wpdb->get_results("$query_from1");
			$label_sub_attr = $wpdb->get_results("$query_from2");
		echo '<div>';
			foreach ($label_attr as $v1) {
				foreach ($v1 as $v2) {
					
						$first = mb_substr($v2,0,1);//первая буква
						$last = mb_substr($v2,1);//все кроме первой буквы
						$first = mb_strtoupper($first);
						$last = strtolower($last);
						$name1 = $first.$last;
						echo $name1;
				}
			}
			echo ': ';
			foreach ($label_sub_attr as $v1) {
				foreach ($v1 as $v2) {
					echo "$v2";
				}
			}
			echo '</div>';
		}
}

add_action( 'wp_ajax_trach_reply_forum', 'trach_reply_in_forum' );
add_action('wp_ajax_nopriv_trach_reply_forum', 'trach_reply_in_forum');

function trach_reply_in_forum() {
	global $woocommerce;
	$id_reply = $_POST['id_reply'];
	global $wpdb;
	wp_delete_post($id_reply);
	echo '<pre>'.$id_reply.'</pre>';

}


add_action( 'wp_ajax_add_cart_to_korzina', 'add_product_to_korzina' );
add_action('wp_ajax_nopriv_add_cart_to_korzina', 'add_product_to_korzina');

function add_product_to_korzina() {
	global $woocommerce;
	$product_id = $_POST['id_product'];
	$quantity = $_POST['value_product'];
	echo '<pre>'.$product_id.'</pre>';
	echo '<pre>'.$quantity.'</pre>';
	 try{
	 	$woocommerce->cart->add_to_cart( $product_id, $quantity);
		}
		 catch ( Exception $e ) {
		   if ( $e->getMessage() ) {
			   wc_add_notice( $e->getMessage(), 'error' );
		   }
		   return false;
	   }
}

function FilterProd($id_attr){
	global $wpdb;
	for($i = 0; $i < count($id_attr); $i++){
	$query_from = "SELECT `slug` FROM `im_terms` WHERE `term_id` = '$id_attr[$i]'";
	$label_attr[$i] = $wpdb->get_results("$query_from");
	//cl_print_r($label_attr[0]->slug);
	}
	return $label_attr;
}

// function cl_print_r ($var, $label = '')
// {
// 	$str = json_encode(print_r ($var, true));
// 	echo "<script>console.group('".$label."');console.log('".$str."');console.groupEnd();</script>";
// }

function trim_characters($str, $count, $after = '...'){
	$excerpt = $str;
	$excerpt = strip_tags($excerpt);
	$excerpt = mb_substr($excerpt, 0, $count);
	$excerpt = $excerpt . $after;
	return $excerpt;
  }

  function get_id_forum(){
	  global $wpdb;

	  $query = "SELECT `ID` FROM `im_posts` WHERE `post_type`= 'forum' AND `post_parent`=0 AND `post_status`= 'publish'";
	  $res = $wpdb->get_results("$query");
	  return $res;
  }
  