<?php
use LisDev\Delivery\NovaPoshtaApi2;
/**
 * Checkout billing information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-billing.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.9
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/** @global WC_Checkout $checkout */

?>

	<div id="success_send" style="display:none; ">
		<div style="width: 50px; height: 50px;">
			<img src="<?php echo get_template_directory_uri() ?>/inc/urich/img/success_send_mess.png" alt="">
		</div>
		<label style="margin-top: 20px; margin-left: 10px;">Ваша заявка отправлена в обработку</label>
	</div>
	<div id="Error_empty_data" style="display:none;">
		<div style="width: 50px; height: 50px;">
			<img src="<?php echo get_template_directory_uri() ?>/inc/urich/img/error_send_mess.png" alt="">
		</div>
		<label style="margin-top: 15px; margin-left: 10px;">Заполните пожалуйста все поля</label>
	</div>
<div class="woocommerce-billing-fields" style="wi">
	<?php if ( wc_ship_to_billing_address_only() && WC()->cart->needs_shipping() ) : ?>

	<h2 class="header-section">ОФОРМЛЕНИЕ ЗАКАЗА</h2>

	<?php else : ?>

	<h2 class="header-section">ОФОРМЛЕНИЕ ЗАКАЗА</h2>

	<?php endif; ?>

	<?php do_action( 'woocommerce_before_checkout_billing_form', $checkout ); ?>

	<div class="woocommerce-billing-fields__field-wrapper">

	<section class="checkout row">
            <div class='col-xl-9 col-lg-9 col-md-10 col-sm-12 col-12 mx-auto'>
			<div class="form-group">
		<?php
			$fields = $checkout->get_checkout_fields( 'billing' );
			foreach ( $fields as $key => $field ) {
				if ( isset( $field['country_field'], $fields[ $field['country_field'] ] ) ) {
					$field['country'] = $checkout->get_value( $field['country_field'] );
				}
				//echo $checkout->get_value( $key ). ' ';
				woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
			} ?>
			</div>
			<div class="form-group">
				<select class="form-control mb-2 delivery" id="exampleFormControlSelect1">
					<option>Выберите способ доставки</option>
					<option>Новая почта</option>
					<option>УкрПочта</option>
					<option>Самовывоз в Киеве</option>
				</select>

				<select class="form-control mb-2 delivery_sity" id="exampleFormControlSelect3" style="display:none">
				<option>Выберите город доставки</option>
					<?php citys_nova_poshta(); ?>
				</select>

				<span class="woocommerce-input-wrapper form-control mb-2" id="number_otd_NP" style="display:none">
					<input style="border-style: none; width: 100%;" type="text" class="input-text " id="exampleFormControlSelect4" placeholder="Введите номер отделения" value="">
				</span>

				<span class="woocommerce-input-wrapper form-control mb-2" id="number_index" style="display:none">
					<input style="border-style: none; width: 100%;" type="text" class="input-text "  id="exampleFormControlSelect5" placeholder="Введите почтовый индекс" value="">
				</span>

				<span class="woocommerce-input-wrapper form-control mb-2" id="address_delivery" style="display:none">
					<input style="border-style: none; width: 100%;" type="text" class="input-text " id="exampleFormControlSelect6" placeholder="Введите полный адрес" value="">
				</span>
				<select class="form-control mb-2 payment" id="exampleFormControlSelect2">
					<option>Выберите способ оплаты</option>
					<option>Предоплата на карту (через кассу, терминал, онлайн или мобильный банк)</option>
					<option>Наложенный платеж (наличными при получении на Новой почте или УкрПочте)</option>
					<option>Безналичный расчет по перечислению от предприятия</option>
				</select>
			</div>
					<div class='mt-3 mb-4'>
                        <p class='m-0 form-text'>Адреса и условия доставки смотрите в <a href='' class='content-link d-inline'>условиях оплаты и доставки.</a></p>
                        <p class='m-0 form-text'>Если вы хотите чтобы доставка осуществлялась каким-то другим способом, укажите его в поле
                            Детали заказа.</p>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1" class='form-label'>Детали заказа</label>
                        <textarea class="form-control " id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
					
		</div>
	</section>

		
	</div>

	<?php do_action( 'woocommerce_after_checkout_billing_form', $checkout ); ?>
</div>
<?php

foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
	$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
	echo '<div class="get_prod_id_class" id="'.$product_id.'"></div>';
	//echo $product_id;
}

//include_once ('/var/www/ivan/data/www/im-tennis.urich.org/wp-content/plugins/woocommerce/templates/cart/cart.php');

// $res = get_id_prod();
// print_r($res);
?>


<?php if ( ! is_user_logged_in() && $checkout->is_registration_enabled() ) : ?>
	<div class="woocommerce-account-fields">
		<?php if ( ! $checkout->is_registration_required() ) : ?>

			<p class="form-row form-row-wide create-account">
				<label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
					<input class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" id="createaccount" <?php checked( ( true === $checkout->get_value( 'createaccount' ) || ( true === apply_filters( 'woocommerce_create_account_default_checked', false ) ) ), true ) ?> type="checkbox" name="createaccount" value="1" /> <span><?php _e( 'Create an account?', 'woocommerce' ); ?></span>
				</label>
			</p>

		<?php endif; ?>

		<?php do_action( 'woocommerce_before_checkout_registration_form', $checkout ); ?>

		<?php if ( $checkout->get_checkout_fields( 'account' ) ) : ?>

			<div class="create-account">
				<?php foreach ( $checkout->get_checkout_fields( 'account' ) as $key => $field ) : ?>
					<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>
				<?php endforeach; ?>
				<div class="clear"></div>
			</div>

		<?php endif; ?>

		<?php do_action( 'woocommerce_after_checkout_registration_form', $checkout ); ?>
	</div>
<?php endif; 

function citys_nova_poshta(){
	$np = new NovaPoshtaApi2(
		'06d0b68541a8cebe354063ab4ef8ff2d',
		'ru', // Язык возвращаемых данных: ru (default) | ua | en
		FALSE, // При ошибке в запросе выбрасывать Exception: FALSE (default) | TRUE
		'curl' // Используемый механизм запроса: curl (defalut) | file_get_content
		);
		$arr = '';
		$i=0;
	$senderWarehouses = $np->getWarehouses($sender['Киев']);
	foreach ($senderWarehouses as $value => $key){
		foreach($key as $v => $k){
		//	echo '<option>'. asort($k['CityDescriptionRu']) . '</option>';
		$arr[$i] = $k['CityDescriptionRu'];
		$i++;
		}
	}
	asort($arr);
	$result = array_unique($arr);
	// echo '<pre>'; print_r($senderWarehouses); echo '</pre>';
	foreach ($result as $value => $key){
	
		echo '<option>'. $key . '</option>';
	}
//	echo '<pre>'; print_r($arr); echo '</pre>';
}


