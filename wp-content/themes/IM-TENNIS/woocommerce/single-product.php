<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>
<link rel="stylesheet" type="text/css" href="slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>
<!-- <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="/var/www/ivan/data/www/im-tennis.urich.org/wp-content/plugins/text/javascript" src="slick/slick.min.js"></script> -->
	<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		
		 */
		
		
		    global $post, $woocommerce;
			$product_id = get_the_ID();
			$args = array ('post_type' => 'product');
            $comments = get_comments( $args );
	     	$product = wc_get_product(get_the_ID());
            $link = get_the_permalink();
            $product_name = $product->get_name();
            $price = $product->get_regular_price() . ' ' . __('грн', THEME_OPT);
            ($product->get_sale_price()) ? $sale_price = $product->get_sale_price() . ' ' . __('грн', THEME_OPT) : $sale_price = null;
            if (has_post_thumbnail()) $product_image = get_the_post_thumbnail_url(get_the_ID(), 'medium');
            $product_buy = $product->add_to_cart_url();
			$product_short_desc = $product->get_short_description();
			$product_attribut = $product->get_attributes();
            $tags = get_the_terms(get_the_ID(), 'product_tag');
            (is_object($tags[0])) ? $tag = __($tags[0]->name, THEME_OPT) : $tag = null;
            if ($tag == 'Хит') $tag_class = 'popular-product-label';
            elseif ($tag == 'Новинка') $tag_class = 'new-products-label';
            elseif ($tag == 'Под заказ') $tag_class = 'order-products-label';
            elseif ($tag == 'Акция') {
                $tag_class = 'promotion-products-label';
                $tag = round(($price - $sale_price) / $price * 100) . '%';
            }

            $product_image_gallery = $product->get_gallery_image_ids( 'view' );

                        $attachments         = array_filter( $product_image_gallery );
                        $update_meta         = false;
                        $updated_gallery_ids = array();

            $stock_status = get_post_meta( $product_id, '_stock_status', true );        
	?>

		<?php while ( have_posts() ) : the_post(); ?>
		<?php 
        include_once '/var/www/ivan/data/www/im-tennis.urich.org/wp-content/plugins/woocommerce/includes/admin/meta-boxes/class-wc-meta-box-product-images.php';
        include_once '/var/www/ivan/data/www/im-tennis.urich.org/wp-content/plugins/woocommerce/includes/admin/meta-boxes/views/html-product-add-products-video.php';
        $post = new WC_Meta_Box_Product_Images();
        $stack_img = array();
        $stack_img = $post->output2($product_id);

?>
		<div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="http://im-tennis.urich.org/shop/">Магазин</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $product_name ?></li>
            </ol>
        </nav>
        <h2 class="header-section"><?php echo $product_name; ?></h2>
        <section class="card_shop row">
            <div class='col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 '>
                <div class=''>
                    <div class='slider-for' style="height: 435px" >
                    <?php
                if ( ! empty( $attachments ) ) {
                            foreach ( $attachments as $attachment_id ) {
                                $attachment = wp_get_attachment_image( $attachment_id, 'thumbnail' );
        
                                // if attachment is empty skip.
                                if ( empty( $attachment ) ) {
                                    $update_meta = true;
                                    continue;
                                }
        
                                echo '<div class="slick-slider-slide" data-attachment_id="' . esc_attr( $attachment_id ) . '">
                                        ' . $attachment . '
                                    </div>';
        
                                // rebuild ids to be saved.
                                $updated_gallery_ids[] = $attachment_id;
                            }
                        }
                ?>
                        <div class='slick-slider-slide' style="height: 100%"><img style="height: 100%" src="<?php echo $product_image; ?>" alt=""></div>
                    </div>

                    <div class="card-label promotion-products-label"><?php echo $tag ?></div>
                </div>
                <div class='slick-slider-card'>
                
                    <?php
                if ( ! empty( $attachments ) ) {
                            foreach ( $attachments as $attachment_id ) {
                                $attachment = wp_get_attachment_image( $attachment_id, 'thumbnail' );
                                   // echo count($attachments);
                                    if(count($attachments)>1){
                                // if attachment is empty skip.
                                if ( empty( $attachment ) ) {
                                    $update_meta = true;
                                    continue;
                                }
        
                                echo '<div class="slick-slider-card-slide" data-attachment_id="' . esc_attr( $attachment_id ) . '">
                                        ' . $attachment . '
                                    </div>';
        
                                // rebuild ids to be saved.
                                $updated_gallery_ids[] = $attachment_id;
                            }
                            }
                        }
                ?>
               
                </div>
                
 <script>
     document.addEventListener("DOMContentLoaded", function(){
 var item = document.getElementsByClassName('slick-slider-slide slick-slide slick-current slick-active');
 if(item){
     console.log("1111");
     var counter = document.querySelectorAll('.slick-slider-card-slide').length;
     var shownDivs = 3; 
     var block = $(".slick-slider-card-slide");
     document

 }
     });

</script> 
            </div>
            <div class='col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 card_shop-about'>
            
                <?php 
                //echo $stock_status;
                if($stock_status == 'instock') 
                { ?>
                <div class='card_shop-about-availability'>&#10004; Есть в наличии</div>
                <?php
                }
                 else 
                 { ?>
                <div class='card_shop-about-availability'>&#10004; Нет в наличии</div>
                <?php
                } ?>
                <div>
                    <span class='card_shop-about-info'> Форма ручки:</span>
                    <nav class='mt-2'>
                        <div class="nav card_shop-list flex-nowrap w-100" id="nav-tab" role="tablist">
                            <a class="nav-item card_shop-link active" id="nav-home-tab" data-toggle="tab" href="" role="tab"
                                 aria-selected="true">Расклешенная</a>
                            <a class="nav-item card_shop-link" id="nav-profile-tab" data-toggle="tab" href="" role="tab"
                                 aria-selected="false">Прямая</a>
                            <a class="nav-item card_shop-link" id="nav-contact-tab" data-toggle="tab" href="" role="tab"
                                 aria-selected="false">Китайское перо</a>
                        </div>
                    </nav>
                </div>
                <div class='card_shop-about-price'>
                    <!-- <a href="" class='card_shop-about-price-article'>Какие бывают формы ручки?</a> -->
                    <div class='mt-4'>
                        <div class="price d-flex align-items-center">
                            <?php if($sale_price!=0) {?>
                                <span class='text-success mr-3'>
                                    <?php echo $sale_price; ?>
                                </span>
                                <span class='price-secondary'>
                                    <?php  echo $price; ?>
                                </span>
                            <?php
                            }
                            else{
                                ?>
                                <span class='text-success mr-3'>
                                    <?php echo $price; ?>
                                </span>
                            <?php
                            }
                            ?>
                        </div>
                        <div class="d-flex align-items-center ">
                            <input id="<?php echo $product_id; ?>" type="number" min="1" value='1' class='cart-card-input add-to-cart'>
                            <div class=' col-xl-5 col-lg-5 col-md-4 col-sm-5 col-6 p-0 ml-3'>
                                <a href="" class="content-btn w-100 d-block">Купить</a></div>
                        </div>
                    </div>
                    <div class='mt-2'>
                        <span class='card_shop-about-price-text'> Или заказать по телефону: </span>
                        <a href="tel:<?php echo $mytheme['header-tel-1'] ?>" class='card_shop-about-price-text'>+38 099 147 18 64</a>,
                         <a href="tel:<?php echo $mytheme['header-tel-2'] ?>" class='card_shop-about-price-text'>+38 095 444 72 99</a>

                      
                    </div>
                </div>
            </div>
            <div class="col-12 mt-4 card_shop-info">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link tab-link active mr-3" id="nav-home-tab" data-toggle="tab" href="#nav-home"
                            role="tab" aria-controls="nav-home" aria-selected="true">Описание</a>
                        <a class="nav-item nav-link tab-link mr-3" id="nav-profile-tab" data-toggle="tab" href="#nav-profile"
                            role="tab" aria-controls="nav-profile" aria-selected="false">Характеристики</a>
                        <a class="nav-item nav-link tab-link mr-3" id="nav-contact-tab" data-toggle="tab" href="#nav-video"
                            role="tab" aria-controls="nav-contact" aria-selected="false">Видео</a>
                        <a class="nav-item nav-link tab-link mr-3" id="nav-contact-tab" data-toggle="tab" href="#nav-review"
                            role="tab" aria-controls="nav-contact" aria-selected="false">Отзывы</a>
                    </div>
                </nav>
                <div class="tab-content mt-4" id="nav-tabContent" style="heigth: 150px">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    
                        <p><?php echo $product_short_desc; ?></p>
                        <?php
                          
                        
                        ?>
                    </div>
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
					<p><?php
                     $j=0;
                     $k=0;
                    foreach( $product_attribut as $attribute_taxonomy => $product_attribute){
                        $name = $product_attribute->get_name();
                        $attribute_data = $product_attribute->get_data();
                         $i=0;
                         $arr_name[$k] = $name;
                        foreach($attribute_data as $val=>$key){
                           if($i==2)
                           {
                              $arr[$j] = $key[0];
                               $j++;
                           }
                           $i++;
                        }
                     $k++;
                    }
                   // echo '<pre>'; print_r( $arr_name ); echo '</pre>'; 
                    get_attr_by_product($arr_name,$arr);
                    
					?></p>
					</div>
                    <div class="tab-pane fade" id="nav-video" role="tabpanel" aria-labelledby="nav-contact-tab">
                        <?php
                           $url = get_post_meta( $product_id, '_product_video', true );
                           if(!empty($url))
                           {
                                //$url = 'https://www.youtube.com/watch?v=yjHzCtHY56k';
                                $parsed_url = parse_url($url);
                                parse_str($parsed_url['query'], $parsed_query);
                                echo '<iframe src="http://www.youtube.com/embed/' . $parsed_query['v'] . '" type="text/html" width="400" height="300" frameborder="0"></iframe>';
                            }
                            else
                            echo 'Видео по данному товару временно отсутствует!';
                     ?>
                    </div>
                    <div class="tab-pane fade" id="nav-review" role="tabpanel" aria-labelledby="nav-contact-tab">
							  <?php 
							  
							  ?>
                    </div>
                </div>
            </div>
        </section>
    </div>

		<?php endwhile; // end of the loop. ?>



<?php get_footer( 'shop' );
function pr($var) {
    
        foreach($var as $value)
        {
            echo '<pre>';
            echo $value->get_name(). ' :   '. $value->get_options()[0];
            echo '</pre>';
        }
}
function pr2($var) {
    $x = 0;
    foreach($var as $value)
    {
        echo '<pre>';
        echo $value;
        echo '</pre>';
        $x++;
    }
    echo $x;
    
}
function skleika($var, $img_post)
{
     $x = 0;
     foreach($var as $value)
     {
         $value;
         $x++;
     }

     for($i=$x+1; $i<$x+2; $i++){

        $var[$i] = $img_post;
     }
     foreach($var as $value)
     {
         echo '<pre>';
         echo $value;
         echo '</pre>';
         $x++;
     }
     echo $x;
}

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
