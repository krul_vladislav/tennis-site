<?php
if(isset($_GET['select'])){
    if ($_GET['select'] == 'newest') { $order = "&orderby=date&order=DESC"; $s1 = ' selected="selected"'; }
    if ($_GET['select'] == 'lastest') { $order = "&orderby=date&order=ASC"; $s2 = ' selected="selected"'; }
    if ($_GET['select'] == 'title') { $order = "&orderby=title&order=ASC"; $s3 = ' selected="selected"'; }
    if ($_GET['select'] == 'correct') { $order = "&orderby=modified"; $s4 = ' selected="selected"'; }
}
?>
<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

do_action('genesis_after_entry');

?>

<style>
    .display-none{
        display: none! important;
    }
</style>
<div class="container">
   <?php get_template_part('templates/breadcrumb', null); ?>
    
    <?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
        <h2 class="header-section"><?php woocommerce_page_title(); ?></h2>
        <section class="category_block d-flex flex-wrap">
         
                    <div class="filter-block col-lg-3 col-md-4 col-sm-12 col-xs-12 col-12 filter-form">
                        <div id="filter_span_parent" class='d-flex flex-wrap mb-2' style="padding-top: 15px;">
                        </div>
                        <?php 
                            dynamic_sidebar('widget-area-1');
                        ?>
                            <div class="d-flex justify-content-center mt-4 mb-4">
                                <a class="filter-link" href="http://im-tennis.urich.org/shop/">сбросить фильтр</a>
                            </div>
                    </div>
            <?php
                $modeType = 'block';
                if(isset($_COOKIE['prod-type-view']) && $_COOKIE['prod-type-view'] == 'list'){
                    $modeType = 'list';
                }                
            ?>
            <div class="products-block col-lg-9 col-md-8 col-sm-12 col-12 pr-0">
               <div class='d-flex justify-content-between align-items-center mb-4'>

               <div class="form-group products-filter-select col-xl-4 col-lg-4 col-md-8 col-sm-10 col-12 pl-0 mb-0">
                    <form method="get" id="order">
                    <select class="form-control" name="select" onchange='this.form.submit()' style="width:200px">
                    <option value="newest">по дате (сначала новые)</option>
                    <option value="lastest">по дате (сначала старые)</option>
                    <option value="title">по заголовку</option>
                    <option value="correct">по дате изменения</option>
                    </select>
                    </form>
                </div>
                    <?php if(isset($_GET['select'])){
                    global $query_string; // параметры базового запроса
                    query_posts($query_string.'&'.$order); // базовый запрос + свои параметры
                    }
                    ?>
                    <div class="d-none d-xl-flex d-lg-flex d-md-flex d-sm-flex align-items-center justify-content-end" style="width: 100%">
                    
                        <div class="products-filter-mode d-flex align-items-center justify-content-center active <?php if($modeType != 'block') echo 'cust-no-active' ?>" id="prod_block" onclick="filterMode(this, 'block')">
                            <img src="<?php echo get_template_directory_uri() ?>/inc/urich/img/3.svg" alt=""></div>
                        <div class="products-filter-mode d-flex align-items-center justify-content-center active <?php if($modeType == 'block') echo 'cust-no-active' ?>" id="prod_list" onclick="filterMode(this, 'list')">
                            <img src="<?php echo get_template_directory_uri() ?>/inc/urich/img/4.svg" alt=""></div>
                    </div>
                    
                </div>
                <?php                              
                if($modeType == 'list'){
                    get_template_part( 'templates/loop-products-list', null );
                   
                }else{
                    get_template_part( 'templates/loop-products', null );
                }                
                
                if ( woocommerce_product_loop() ) {
                    do_action( 'woocommerce_after_shop_loop' );
                } else {
                    do_action( 'woocommerce_no_products_found' );
                }
                do_action( 'woocommerce_after_main_content' );
                ?>
                	
            </div>
        </section>
	<?php endif; ?>

	<?php
	do_action( 'woocommerce_archive_description' );
	?>


</div>

<?php
get_footer( 'shop' );
?>

