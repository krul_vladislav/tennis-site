<?php // Template Name: Page forum ?>

<?php get_header(); ?>

<div class="container">

<nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="http://im-tennis.urich.org/shop/">Магазин</a></li>
                <li class="breadcrumb-item active" aria-current="page">Форум</li>
            </ol>
        </nav>
        <h2 class="header-section">ФОРУМ</h2>
        <?php if ( bbp_is_forum_edit() ) : ?>
        <section class="forum mb-5">
        
               <?php bbp_breadcrumb();
               // $x = array (); ?>
                 <?php endif; ?>
                 <?php include_once '/var/www/ivan/data/www/im-tennis.urich.org/wp-content/plugins/bbpress/includes/forums/template.php';?>
                 <?php include_once '/var/www/ivan/data/www/im-tennis.urich.org/wp-content/plugins/bbpress/includes/forums/functions.php';?>
                 <?php include_once '/var/www/ivan/data/www/im-tennis.urich.org/wp-content/plugins/bbpress/includes/core/template-functions.php';?>
                 <?php include_once '/var/www/ivan/data/www/im-tennis.urich.org/wp-content/plugins/bbpress/includes/core/capabilities.php';?>
                 
            <div class="accordion" id="accordionExample">
                     
           
                <?php
                 if (bbp_forums()!=0)
                 {
                    echo '111';
                 }


                 $arr = get_id_forum();
                   for($i=0; $i<count($arr);$i++){
                    $forum_id[$i] = $arr[$i]->ID; 
                   }
                   // cl_print_r($forum_id);
                 do_action( 'bbp_template_before_forums_loop' ); 
                 //print_r (bbp_get_forum_post_type_labels());
                
                 

                 for($cycle_id =0; $cycle_id<count($forum_id);$cycle_id++){
                ?> 
                
                <div class='forum-item'>
                    <a data-toggle="collapse" href="#collapse<?php echo $cycle_id; ?>" class="forum-item-header d-flex justify-content-between align-items-center collapsed"> <?php bbp_forum_title($forum_id[$cycle_id]); ?>
                        <span class="pr-4"></span> <span class='forum-arrow'></span>
                    </a>
                    <?php do_action( 'bbp_template_before_forums_loop' ); ?>
                    <div id="collapse<?php echo $cycle_id; ?>" class="collapse">
                        <?php
                        $x = 0;
                        global $subforum_ids;
                        $subforum_ids = bbp_get_all_child_ids( $forum_id[$cycle_id], bbp_get_forum_post_type() );
                        
                        for($x;$x<count($subforum_ids);$x++) {
                            ?>
                             <ul class="list-unstyled mt-3">
                            <li class="media align-items-center">
                                <div style='background-image: url(img/forum.png)' class='media-img '></div>
                                <div class="media-body">
                                <?php 
                                
                               
                                ?>
                                
                                <!-- <?php bbp_forum_permalink($subforum_ids[$x]); ?> -->
                                    <a href='http://im-tennis.urich.org/forum-list/'>
                                    <a class="mt-0 mb-1 media-body-header" href="<?php bbp_forum_permalink($subforum_ids[$x]); ?>"><?php bbp_forum_title($subforum_ids[$x]); ?></a>
                                        <!--  -->
                                    </a>
                                    <div class='media-body-text'>
                                        Последний ответ от <a href='' class='content-link d-inline'><?php bbp_forum_last_topic_author_link($subforum_ids[$x]); ?></a> в <a
                                            href='' class='content-link d-inline'>Re: <?php bbp_forum_last_topic_title($subforum_ids[$x]); ?> </a> <span class='media-body-text-date'> <?php bbp_forum_last_active_time( $subforum_ids[$x]); ?></span>
                                    </div>
                                </div>
                                <div class='media-body-text text-right'>
                                    <span class='d-block'><?php bbp_forum_post_count($subforum_ids[$x]); ?></span>
                                    <span><?php bbp_forum_topic_count($subforum_ids[$x]); ?></span>
                                </div>
                            </li>
                            <?php
                        }
                        ?>
                        </ul>
                    </div>
                </div>
                <?php
                 }
                ?>
            </div>
        </section>
    
	<!-- /section -->
    </div>
<?php get_footer(); ?>